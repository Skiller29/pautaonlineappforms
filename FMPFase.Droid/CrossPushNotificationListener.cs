﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using PushNotification.Plugin;
using PushNotification.Plugin.Abstractions;
using pautaonlineforms;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Library.Droid;
using pautaonlineforms.Services;

namespace FMPFase.Droid
{
    public class CrossPushNotificationListener : IPushNotificationListener
    {
        public void OnError(string message, DeviceType deviceType)
        {
            //throw new NotImplementedException();
        }

        public void OnMessage(JObject values, DeviceType deviceType)
        {
            try
            {
                Intent intent = new Intent(Android.App.Application.Context, typeof(MainActivity));
                Intent[] intar = { intent };
                PendingIntent pintent = PendingIntent.GetActivities(Android.App.Application.Context, 0, intar, 0);
                intent.SetAction("notification");

                //var sound = Android.Net.Uri.Parse("android.resource://com.myPackageName.org/" + Resource.Drawable.notification);               
                //Json Response Recieved 
                Dictionary<string, string> results = JsonConvert.DeserializeObject<Dictionary<string, string>>(values.ToString());
                var notificacao = new pautaonlineforms.Entities.Notification()
                {
                    AgendaName = results["AgendaName"],
                    SenderName = results["SenderName"],
                    Content = results["Content"],
                    SentIn = DateTime.Now

                };
                var title = $"{notificacao.SenderName} - {notificacao.AgendaName}";
                Notification.Builder builder = new Notification.Builder(Android.App.Application.Context)
                 .SetContentTitle(title)
                 .SetContentText(notificacao.Content)
                 .SetContentIntent(pintent)
                 .SetSmallIcon(Resource.Drawable.ic_launcher);

                // Build the notification:
                Notification notification = builder.Build();

                //Clear notification on click
                notification.Flags = NotificationFlags.AutoCancel;

                // Get the notification manager:
                NotificationManager notificationManager = Android.App.Application.Context.GetSystemService(PushNotificationService.NotificationService) as NotificationManager;

                //notificationId  need to be unique for each message same ID will update existing message

                // Publish the notification:
                notificationManager.Notify(new Random().Next(1, 1000), notification);
                NotificationsService.Save(notificacao);
            }
            catch (Exception)
            {
            }
        }

        public void OnRegistered(string token, DeviceType deviceType)
        {
            App.GetToken(token, "Android");
        }

        public void OnUnregistered(DeviceType deviceType)
        {
            //throw new NotImplementedException();
        }

        public bool ShouldShowNotification()
        {
            return false;
        }
    }
}