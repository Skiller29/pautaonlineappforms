﻿using FMPFase.Droid;
using pautaonlineforms.Entities;
using pautaonlineforms.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(ClientAppImplementation))]
namespace FMPFase.Droid
{
    public class ClientAppImplementation : IClientApp
    {
        public ClientApp GetClient()
        {
            var cliente = new ClientApp()
            {
                AppName = "FMP FASE",
                HexaAppColor = "#00706B",
                IdInstitute = 1,
                LogoName = "logo_fmpfase.png"
            };

            return cliente;
        }
    }
}