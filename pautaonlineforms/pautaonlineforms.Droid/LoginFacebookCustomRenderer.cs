using System;
using Android.App;
using Newtonsoft.Json.Linq;
using Xamarin.Auth;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using pautaonlineforms.Droid;
using pautaonlineforms.Views;

[assembly: ExportRenderer(typeof(LoginFacebookPage), typeof(LoginFacebookCustomRenderer))]
namespace pautaonlineforms.Droid
{
    public class LoginFacebookCustomRenderer : PageRenderer
    {

        public LoginFacebookCustomRenderer()
        {
            var activity = this.Context as Activity;

            var auth = new OAuth2Authenticator(
                clientId: "212660225441375", // your OAuth2 client id
                scope: "email", // the scopes for the particular API you're accessing, delimited by "+" symbols
                authorizeUrl: new Uri("https://www.facebook.com/v2.8/dialog/oauth/"),
                redirectUrl: new Uri("https://www.facebook.com/connect/login_success.html"));

            auth.Completed += async (sender, eventArgs) =>
            {
                if (eventArgs.IsAuthenticated)
                {

                    var accessToken = eventArgs.Account.Properties["access_token"].ToString();
                    var expiresIn = Convert.ToDouble(eventArgs.Account.Properties["expires_in"]);
                    var expiryDate = DateTime.Now + TimeSpan.FromSeconds(expiresIn);

                    var request = new OAuth2Request("GET",
                        new Uri("https://graph.facebook.com/me?fields=id,email,name,picture"), null, eventArgs.Account);
                    var response = await request.GetResponseAsync();
                    var obj = JObject.Parse(response.GetResponseText());

                    var id = obj["id"].ToString().Replace("\"", "");
                    var email = obj["email"].ToString().Replace("\"", "");
                    var name = obj["name"].ToString().Replace("\"", "");
                    var foto = "http://graph.facebook.com/" + id + "/picture?type=large";
                    await App.AuthenticationFacebook(id, name, email, foto);
                }
            };
            activity.StartActivity(auth.GetUI(activity));
        }
    }
}