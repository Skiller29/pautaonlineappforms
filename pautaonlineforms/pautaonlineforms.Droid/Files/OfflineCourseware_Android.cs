﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.ComponentModel;
using Acr.UserDialogs;
using pautaonlineforms.Interfaces;

namespace pautaonlineforms.Droid.Files
{
    public class OfflineCourseware_Android : IOfflineCourseware
    {
        public bool FileExists(string filename)
        {
            return File.Exists(CreatePathToFile(filename));
        }

        public string Load(string filename)
        {
            if(FileExists(filename))
                return CreatePathToFile(filename);

            return null;
        }

        public string Save(string filename, string email, string url)
        {
            var name = Path.Combine(email, filename);
            var path = CreatePathToFile(name);

            Download(url, path);
            return path;
        }

        string CreatePathToFile(string filename)
        {
            var docsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(docsPath, filename);
        }

        void Download(string url, string path)
        {
            using (WebClient myWebClient = new WebClient())
            {
                myWebClient.DownloadFileCompleted += DownloadCompleted;
                myWebClient.DownloadFileAsync(new Uri(url), path);
            }
        }

        public void Delete(string filename)
        {
            var path = CreatePathToFile(filename);
            if (FileExists(path))
                File.Delete(path);
        }

        public static void DownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            UserDialogs.Instance.Toast("Download completo", new TimeSpan(0, 0, 4));
        }

        public Stream GetStreamPDF(string url)
        {
            using (WebClient myWebClient = new WebClient())
            {
                return myWebClient.OpenRead(new Uri(url));
            }
        }
    }
}