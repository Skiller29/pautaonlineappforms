﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PushNotification.Plugin;
using PushNotification.Plugin.Abstractions;
using System.Xml.Linq;
using Android.Media;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace pautaonlineforms.Droid
{
    public class CrossPushNotificationListener : IPushNotificationListener
    {
        public void OnError(string message, DeviceType deviceType)
        {
            //throw new NotImplementedException();
        }

        public void OnMessage(JObject values, DeviceType deviceType)
        {
            try
            {
                Intent intent = new Intent(Android.App.Application.Context, typeof(MainActivity));
                Intent[] intar = { intent };
                PendingIntent pintent = PendingIntent.GetActivities(Android.App.Application.Context, 0, intar, 0);
                intent.SetAction("notification");

                //var sound = Android.Net.Uri.Parse("android.resource://com.myPackageName.org/" + Resource.Drawable.notification);

                //Json Response Recieved 
                Dictionary<string, string> results = JsonConvert.DeserializeObject<Dictionary<string, string>>(values.ToString());
                Notification.Builder builder = new Notification.Builder(Android.App.Application.Context)
                 .SetContentTitle(results["title"])
                 .SetContentText(results["message"])                 
                 .SetContentIntent(pintent)                 
                 .SetSmallIcon(Resource.Drawable.icon);

                // Build the notification:
                Notification notification = builder.Build();

                //Clear notification on click
                notification.Flags = NotificationFlags.AutoCancel;

                // Get the notification manager:
                NotificationManager notificationManager = Android.App.Application.Context.GetSystemService(PushNotificationService.NotificationService) as NotificationManager;
               
                //notificationId  need to be unique for each message same ID will update existing message
                int notificationId = Convert.ToInt32(results["Id"]);
                // Publish the notification:
                notificationManager.Notify(notificationId, notification);
            }
            catch (Exception)
            {
            }
        }

        public void OnRegistered(string token, DeviceType deviceType)
        {
            App.GetToken(token, "Android");
        }

        public void OnUnregistered(DeviceType deviceType)
        {
            //throw new NotImplementedException();
        }

        public bool ShouldShowNotification()
        {
            return false;
        }

        private void CreateAndShowDialog(string message, string title, string typeNotification, string reference)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(Application.Context);

            builder.SetMessage(message);
            builder.SetTitle(title);
            builder.SetIcon(Resource.Drawable.icon);
            builder.SetCancelable(false);
            builder.Create().Show();
        }
    }
}