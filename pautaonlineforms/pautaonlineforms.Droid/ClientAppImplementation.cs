﻿using pautaonlineforms.Droid;
using pautaonlineforms.Entities;
using pautaonlineforms.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(ClientAppImplementation))]
namespace pautaonlineforms.Droid
{
    public class ClientAppImplementation : IClientApp
    {
        public ClientApp GetClient()
        {
            var cliente = new ClientApp()
            {
                AppName = "Pauta Online",
                HexaAppColor = "#FD8726",
                IdInstitute = 0,
                LogoName = "logo.png"
            };

            return cliente;
        }
    }
}