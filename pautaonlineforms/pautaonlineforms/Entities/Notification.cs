﻿using SQLite.Net.Attributes;
using System;


namespace pautaonlineforms.Entities
{
    public class Notification
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string MemberActivationToken { get; set; }
        public string AgendaName { get; set; }
        public string SenderName { get; set; }
        public string Content { get; set; }
        public DateTime SentIn { get; set; }

        [Ignore]
        public string FormattedContent
        {
            get { return string.Format("{0}: {1}", this.SenderName, this.Content); }
        }

        public Notification()
        {
        }
    }
}
