﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "States")]
    public class States
    {
        [XmlElement(ElementName = "State")]
        public List<State> State { get; set; }
    }
}
