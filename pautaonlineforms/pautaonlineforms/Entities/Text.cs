﻿using pautaonlineforms.Helpers;
using System;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Text")]
    public class Text
    {
        [XmlElement(ElementName = "Description")]
        public String Description { get; set; }
        [XmlAttribute(AttributeName = "Title")]
        public String Title { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }

        public String AgendaName { get; set; }
        public String ClassName { get; set; }
        public String Thumbnail
        {
            get { return "file_text"; }
        }

        [XmlIgnore]
        public bool IsVisible { get; set; }

        [XmlAttribute(AttributeName = "IsVisible")]
        public string IsVisibleString
        {
            get { return this.IsVisible.ToString(); }
            set
            {
                try
                {
                    this.IsVisible = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.IsVisible = false;
                }
            }
        }

        public String DescriptionCompact { get { return Description.RemoveHtmlTags().LimitSize(40); } }
    }
}
