﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Hierarchies")]
    public class Hierarchies
    {
        [XmlElement(ElementName = "Hierarchy")]
        public List<Hierarchy> Hierarchy;

        public Hierarchies()
        {
            
        }
    }
}
