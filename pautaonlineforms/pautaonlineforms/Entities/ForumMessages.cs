﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;

namespace pautaonlineforms.Entities
{
	[XmlRoot(ElementName="ForumMessages")]
	public class ForumMessages 
	{
		[XmlElement(ElementName="ForumMessage")]
		public List<ForumMessage> ForumMessage { get; set; }
	}
}

