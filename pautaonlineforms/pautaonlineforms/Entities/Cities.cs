﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Cities")]
    public class Cities
    {
        [XmlElement(ElementName = "Cityy")]
        public List<City> City { get; set; }
    }

}
