﻿using System;
using System.Xml.Serialization;
using System.Globalization;
using pautaonlineforms.Helpers;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "ForumMessage")]
    public class ForumMessage
    {
        [XmlElement(ElementName = "Content")]
        public String Content { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }
        [XmlAttribute(AttributeName = "Author")]
        public String Author { get; set; }
        [XmlAttribute(AttributeName = "PhotoUrl")]
        public String PhotoUrl { get; set; }
        [XmlAttribute(AttributeName = "ParentAuthor")]
        public String ParentAuthor { get; set; }
        [XmlAttribute(AttributeName = "Level")]
        public String Level { get; set; }

        [XmlIgnore]
        public DateTime SentIn { get; set; }
        [XmlIgnore]
        public DateTime EditDate { get; set; }

        [XmlElement(ElementName = "SentIn")]
        public String SendInString
        {
            get
            {
                return this.SentIn == DateTime.MinValue ? "" : Convert.ToDateTime(this.SentIn).ToString("dd/MM/yyyy HH:mm", new CultureInfo("pt-BR"));
            }
            set
            {
                try
                {
                    this.SentIn = string.IsNullOrWhiteSpace(value) || value == "Não informado" ? DateTime.MinValue : Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR"));
                }
                catch (Exception)
                {
                    this.SentIn = DateTime.MinValue;
                }
            }
        }

        [XmlElement(ElementName = "EditDate")]
        public String EditDateString
        {
            get
            {
                return this.EditDate == DateTime.MinValue ? "" : this.EditDate.ToString("yyyy-MM-dd");
            }
            set
            {
                try
                {
                    this.EditDate = string.IsNullOrWhiteSpace(value) || value == "Não informado" ? DateTime.MinValue : Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR"));
                }
                catch (Exception)
                {
                    this.EditDate = DateTime.MinValue;
                }
            }
        }

        public string BubbleThumbnail
        {
            get
            {
                return this.IsParent ? "forum_parent" : "forum_child";
            }
        }

        public bool IsParent
        {
            get
            {
                return this.Level == "0";
            }
        }

        public bool IsChild
        {
            get
            {
                return this.Level != "0";
            }
        }

        public string ForumId { get; set; }

        public string ForumTitle { get; set; }

        public string FormattedContent
        {
            get { return Content.RemoveCData().RemoveHtmlTags(); }
        }

    }
}

