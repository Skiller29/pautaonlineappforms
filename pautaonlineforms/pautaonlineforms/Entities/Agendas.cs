﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Agendas")]
    public class Agendas
    {
        [XmlElement(ElementName = "Agenda")]
        public List<Agenda> Agenda;
    }
}
