﻿namespace pautaonlineforms.Entities.Enumerations
{
    public enum MenuSection
    {
        MyAgendasWithInstitutes,
        OfflineCoursewaresWithAgendas,
        MyAccount,
        MyNotificationsWithAgenda,
        Logout
    }
}
