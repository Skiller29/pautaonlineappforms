﻿namespace pautaonlineforms.Entities.Enumerations
{
    public enum OfflineCoursewareType
    {
        Text,
        Attachment,
        Media
    }
}
