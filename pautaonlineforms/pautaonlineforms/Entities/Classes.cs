﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Classes")]
    public class Classes
    {
        [XmlElement(ElementName = "Class")]
        public List<Class> Class;
    }
}
