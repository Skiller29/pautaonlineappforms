﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace pautaonlineforms.Entities
{
	[XmlRoot(ElementName="Errors")]
	public class Errors {
		[XmlElement(ElementName = "Error")]
		public List<String> Error { get; set; }
	}
}

