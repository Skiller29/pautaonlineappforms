﻿using pautaonlineforms.Entities.Enumerations;
using pautaonlineforms.Helpers;
using SQLite.Net.Attributes;
using System.Text.RegularExpressions;


namespace pautaonlineforms.Entities
{
    public class OfflineCourseware
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int SiteId { get; set; }
        public string MemberActivationToken { get; set; }
        public string AgendaName { get; set; }
        public string ClassName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public string Extension { get; set; }
        public OfflineCoursewareType Type { get; set; }

        public string DescriptionCompact { get { return Description.RemoveHtmlTags().LimitSize(40); } }

        [Ignore]
        public string Content { get; set; }
        [Ignore]
        public string NativePath { get; set; }
        [Ignore]
        public byte[] FileInBytes { get; set; }
        [Ignore]
        public string Thumbnail
        {
            get
            {
                switch (this.Type)
                {
                    case OfflineCoursewareType.Text:
                        return "file_text";
                    case OfflineCoursewareType.Attachment:
                        if (this.Extension == "doc" || this.Extension == "docx")
                            return "file_doc";
                        if (this.Extension == "xls" || this.Extension == "xlsx")
                            return "file_xls";
                        if (this.Extension == "ppt" || this.Extension == "pptx")
                            return "file_ppt";
                        if (this.Extension == "jpg" || this.Extension == "jpeg")
                            return "file_jpeg";
                        if (this.Extension == "pdf")
                            return "file_pdf";
                        if (this.Extension == "png")
                            return "file_png";
                        if (this.Extension == "zip")
                            return "file_zip";
                        if (this.Extension == "rar")
                            return "file_rar";
                        if (this.Extension == "mp3")
                            return "file_mp3";
                        return "pautaonline_default";
                    case OfflineCoursewareType.Media:
                        if (this.Extension == "mp3")
                            return "file_mp3";
                        if (this.Extension == "mp4")
                            return "file_mpeg";
                        return "pautaonline_default";
                    default:
                        return "pautaonline_default";
                }
            }
        }
		[Ignore]
		public string TypeDescription
		{
			get {
				switch (this.Type) {
				case OfflineCoursewareType.Text:
					return "Texto";
				case OfflineCoursewareType.Attachment:
					return "Anexo";
				case OfflineCoursewareType.Media:
					return "Mídia";
				default:
					return "Não especificado";
				}
			}
		}

        public OfflineCourseware(Text text, string activationToken, string agendaName)
        {
            this.SiteId = int.Parse(text.SiteId);
            this.MemberActivationToken = activationToken;
            this.AgendaName = agendaName;
            this.ClassName = text.ClassName ?? "";
            this.Title = text.Title;
            this.Description = Regex.Replace(text.Description.RemoveHtmlTags().RemoveNewLineCharacters().LimitSize(120), @"\s*(<[^>]+>)\s*", "$1", RegexOptions.Singleline);
            this.Type = OfflineCoursewareType.Text;
            this.Extension = "txt";
        }

        public OfflineCourseware(Attachment attachment, string activationToken, string agendaName, string path)
        {
            this.SiteId = int.Parse(attachment.SiteId);
            this.MemberActivationToken = activationToken;
            this.AgendaName = agendaName;
            this.ClassName = attachment.ClassName ?? "";
            this.Title = attachment.Title;
            this.FilePath = path;
            this.Description = string.Format("Arquivo {0}", attachment.Extension);
            this.Type = OfflineCoursewareType.Attachment;
            this.Extension = attachment.Extension.ToLower();
        }

        public OfflineCourseware(Media media, string activationToken, string agendaName, string path)
        {
            this.SiteId = int.Parse(media.SiteId);
            this.MemberActivationToken = activationToken;
            this.AgendaName = agendaName;
            this.ClassName = media.ClassName ?? "";
            this.Title = media.Title;
            this.FilePath = path;
            this.Description = media.Description;
            this.Type = OfflineCoursewareType.Media;
            this.Extension = "mp4"; //media.Extension.ToLower();
        }

        public OfflineCourseware()
        {
        }
    }
}
