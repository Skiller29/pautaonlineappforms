﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Institute")]
    public class Institute
    {
        [XmlElement(ElementName = "Hierarchies")]
        public Hierarchies Hierarchies;
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public String Type { get; set; }

        public Institute()
        {
        }
    }
}
