﻿using SQLite.Net.Attributes;
using System.Linq;


namespace pautaonlineforms.Entities
{
    public class OfflineMember
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ActivationToken { get; set; }
        public string ActiveProfile { get; set; }
        [Ignore]
        public string FirstName
        {
            get { return this.FullName.Split(' ').FirstOrDefault(); }
        }
        [Ignore]
        public string LastName
        {
            get { return this.FullName.Split(' ').LastOrDefault(); }
        }

        public OfflineMember()
        {
            
        }

        public OfflineMember(Member member, string senha)
        {
            this.FullName = member.FullName;
            this.Email = member.Email;
            this.Password = senha;
            this.ActivationToken = member.ActivationToken;
            this.ActiveProfile = member.GetActiveProfile().Name;
        }
    }
}
