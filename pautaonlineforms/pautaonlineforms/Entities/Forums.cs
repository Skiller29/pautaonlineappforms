﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace pautaonlineforms.Entities
{
	[XmlRoot(ElementName = "Forums")]
	public class Forums
	{
		[XmlElement(ElementName = "Forum")]
		public List<Forum> Forum { get; set; }

		public bool HasNoForums
		{
			get { return this.Forum.Count == 0; }
		}
	}
}

