﻿using pautaonlineforms.Helpers;
using System;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Link")]
    public class Link
    {
        [XmlElement(ElementName = "Description")]
        public String Description { get; set; }
        //[XmlElement(ElementName = "Thumbnail")]
        [XmlIgnore]
        public String Thumbnail
        {
            get { return "file_url"; }
            set { Thumbnail = value; }
        }
        [XmlAttribute(AttributeName = "Title")]
        public String Title { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public String Type { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }
        [XmlElement(ElementName = "Url")]
        public String Url { get; set; }

        [XmlIgnore]
        public bool IsVisible { get; set; }

        [XmlAttribute(AttributeName = "IsVisible")]
        public string IsVisibleString
        {
            get { return this.IsVisible.ToString(); }
            set
            {
                try
                {
                    this.IsVisible = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.IsVisible = false;
                }
            }
        }

        public String DescriptionCompact { get { return Description.RemoveHtmlTags().LimitSize(40); } }
    }
}
