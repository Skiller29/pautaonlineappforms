﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Hierarchy")]
    public class Hierarchy
    {
        [XmlElement(ElementName = "Agendas")]
        public Agendas Agendas;
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public String Type { get; set; }
        [XmlElement(ElementName = "SubHierarchy")]
        public List<Hierarchy> SubHierarchy;

        public Hierarchy()
        {
        }
    }
}
