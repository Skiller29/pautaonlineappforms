﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Serialization;


namespace pautaonlineforms.Entities
{
    public class Member
    {
       	//[PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Celular { get; set; }
        public string Cep { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Complement { get; set; }
        public string Neighborhood { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Skype { get; set; }
        public string Twitter { get; set; }
        public string Msn { get; set; }
        public string Delicious { get; set; }
        public string Youtube { get; set; }
        public string ActivationToken { get; set; }
        public string Error { get; set; }
        public string Cpf { get; set;}
        public string FacebookId { get; set; }
        public string PhotoUrl { get; set; }
        public Errors Errors;
        public Profiles Profiles;
        public Institutes Institutes;

        [XmlIgnore]
        public bool ReceiveNews { get; set; }
        [XmlIgnore]
        public DateTime BirthDate { get; set; }

        public Member()
        {
        }

        public bool IsValid(bool onlyErrors = false)
        {
            if (onlyErrors)
            {
                if (this == null)
                    return false;
                if (this.Error == null && this.Errors == null)
                    return true;
                return false;
            }
            else
            {
                if (this == null)
                    return false;
                if (this.Error == null && this.Errors == null && this.FullName != null)
                    return true;
                return false;
            }
        }

		public Profile GetActiveProfile()
		{
		    return this.Profiles.Profile.FirstOrDefault(x => x.Active);
		}

        [XmlElement("ReceiveNews")]
        public string ReceiveNewsString
        {
            get { return this.ReceiveNews.ToString(); }
            set
            {
                try
                {
                    this.ReceiveNews = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.ReceiveNews = false;
                }
            }
        }

        [XmlElement("BirthDate")]
        public string BirthDateString
        {
            get
            {
                return this.BirthDate == DateTime.MinValue ? "" : this.BirthDate.ToString("yyyy-MM-dd");
            }
            set
            {
                try
                {
                    this.BirthDate = !string.IsNullOrWhiteSpace(value) ? Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR")) : DateTime.MinValue;
                }
                catch (Exception)
                {
                    this.BirthDate = DateTime.MinValue;
                }
            }
        }

        public List<Agenda> AvailableAgendas
        {
            get
            {
                var agendas = new List<Agenda>();

                try
                {
                    if (this.Institutes != null && this.Institutes.Institute.Any())
                    {
                        foreach (var institute in this.Institutes.Institute)
                        {
                            foreach (var hierarchy in institute.Hierarchies.Hierarchy)
                            {
                                var hierarchyFatherName = hierarchy.Name;
                                var currentHierarchy = hierarchy;
                                if (currentHierarchy.Agendas != null)
                                {
                                    foreach (var agenda in currentHierarchy.Agendas.Agenda)
                                    {
                                        agendas.Add(new Agenda()
                                        {
                                            Name = agenda.Name,
                                            PhotoUrl = agenda.PhotoUrl,
                                            SiteId = agenda.SiteId,
                                            Institute = institute.Name,
                                            Hierarchy = currentHierarchy.Name,
                                            Objectives = agenda.Objectives
                                            //ClassesCounter = agenda.Classes != null ? string.Format("{0} {1}", agenda.Classes.Class.Count(), Mvx.Resolve<ICulturePlugin>().LanguageText("MultiplesClassesText")) : Mvx.Resolve<ICulturePlugin>().LanguageText("ZeroClassText")
                                        });
                                    }
                                }
                                foreach (var subhierarchy in hierarchy.SubHierarchy)
                                {
                                    hierarchyFatherName = hierarchyFatherName + "/" + subhierarchy.Name;
                                    currentHierarchy = subhierarchy;

                                    if (currentHierarchy.Agendas != null)
                                    {
                                        foreach (var agenda in currentHierarchy.Agendas.Agenda)
                                        {
                                            agendas.Add(new Agenda()
                                            {
                                                Name = agenda.Name,
                                                PhotoUrl = agenda.PhotoUrl,
                                                SiteId = agenda.SiteId,
                                                Institute = institute.Name,
                                                Hierarchy = currentHierarchy.Name,
                                                Objectives = agenda.Objectives
                                                //ClassesCounter = agenda.Classes != null ? string.Format("{0} {1}", agenda.Classes.Class.Count(), Mvx.Resolve<ICulturePlugin>().LanguageText("MultiplesClassesText")) : Mvx.Resolve<ICulturePlugin>().LanguageText("ZeroClassText")
                                            });
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception ex)
                {

                }

                return agendas;
            }
        }

        public string FirstName
        {
            get { return this.FullName.Split(' ').FirstOrDefault(); }
        }

        public string LastName
        {
            get { return this.FullName.Split(' ').LastOrDefault(); }
        }
    }
}
