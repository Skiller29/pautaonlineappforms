﻿using pautaonlineforms.Helpers;
using System;
using System.Globalization;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Agenda")]
    public class Agenda
    {
        [XmlElement(ElementName = "Classes")]
        public Classes Classes;
		[XmlElement(ElementName = "Forums")]
		public Forums Forums { get; set; } 
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public String Type { get; set; }
        [XmlAttribute(AttributeName = "PhotoUrl")]
        public String PhotoUrl { get; set; }
        public String AgendaLoad { get; set; }
        public String Price { get; set; }
        public String Teacher { get; set; }
        [XmlIgnore]
        public DateTime DateInitialShow { get; set; }
        [XmlIgnore]
        public DateTime DateFinalShow { get; set; }
        [XmlIgnore]
        public DateTime DateFinalEnrollment { get; set; }
        [XmlIgnore]
        public DateTime StartDate { get; set; }
        public String Objectives { get; set; }
        public String TargetPublic { get; set; }
        public String Content { get; set; }
        public String Bibliography { get; set; }

        public String ObjectivesFormatted
        {
            get { return Objectives.RemoveCData().RemoveHtmlTags(); }
        }

        public String TargetPublicFormatted
        {
            get { return TargetPublic.RemoveCData().RemoveHtmlTags().FormatAccent(); }
        }

        public String ContentFormatted
        {
            get { return Content.RemoveCData().RemoveHtmlTags().FormatAccent(); }
        }

        public String BibliographyFormatted
        {
            get { return Bibliography.RemoveCData().RemoveHtmlTags().FormatAccent(); }
        }

        public String Error { get; set; }

        public string Institute { get; set; }
        public string Hierarchy { get; set; }
        public string ClassesCounter { get  { return Classes.Class.Count.ToString(); }
        }

		[XmlIgnore]
		public bool ChatActive { get; set; }

		[XmlIgnore]
		public bool ChatNotActive 
		{ 
			get {
				return !this.ChatActive;
			}
		}


        [XmlElement("DateInitialShow")]
        public string DateInitialShowString
        {
            get { return this.DateInitialShow.ToString("yyyy-mm-dd"); }
            set
            {
                try
                {
                    this.DateInitialShow = string.IsNullOrWhiteSpace(value) || value == "Não informado" ? DateTime.MinValue : Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR"));
                }
                catch (Exception)
                {
                    this.DateInitialShow = DateTime.MinValue;
                }
            }
        }

        [XmlElement("DateFinalShow")]
        public string DateFinalShowString
        {
            get { return this.DateFinalShow.ToString("yyyy-mm-dd"); }
            set
            {
                try
                {
                    this.DateFinalShow = string.IsNullOrWhiteSpace(value) || value == "Não informado" ? DateTime.MinValue : Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR"));
                }
                catch (Exception)
                {
                    this.DateInitialShow = DateTime.MinValue;
                }
            }
        }

        [XmlElement("DateFinalEnrollment")]
        public string DateFinalEnrollmentString
        {
            get { return this.DateFinalEnrollment.ToString("yyyy-mm-dd"); }
            set
            {
                try
                {
                    this.DateFinalEnrollment = string.IsNullOrWhiteSpace(value) || value == "Não informado" ? DateTime.MinValue : Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR"));
                }
                catch (Exception)
                {
                    this.DateInitialShow = DateTime.MinValue;
                }
            }
        }

        [XmlElement("StartDate")]
        public string StartDateString
        {
            get { return this.StartDate.ToString("yyyy-mm-dd"); }
            set
            {
                try
                {
                    this.StartDate = string.IsNullOrWhiteSpace(value) || value == "Não informado" ? DateTime.MinValue : Convert.ToDateTime(value, new System.Globalization.CultureInfo("pt-BR"));
                }
                catch (Exception)
                {
                    this.StartDate = DateTime.MinValue;
                }
            }
        }

		[XmlElement("ChatActive")]
		public string ChatActiveString
		{
			get { return this.ChatActive.ToString(); }
			set
			{
				try
				{
					this.ChatActive = Boolean.Parse(value);
				}
				catch (Exception)
				{
					this.ChatActive = false;
				}
			}
		}
    }
}
