﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Coursewares")]
    public class Coursewares
    {
        [XmlElement(ElementName = "Attachment")]
        public List<Attachment> Attachment { get; set; }
        [XmlElement(ElementName = "Text")]
        public List<Text> Text { get; set; }
        [XmlElement(ElementName = "Link")]
        public List<Link> Link { get; set; }
        [XmlElement(ElementName = "Media")]
        public List<Media> Media { get; set; }

        public bool HasNoAttachments
        {
            get { return this.Attachment.Count == 0; }
        }
        public bool HasNoTexts
        {
            get { return this.Text.Count == 0; }
        }
        public bool HasNoLinks
        {
            get { return this.Link.Count == 0; }
        }
        public bool HasNoMedias
        {
            get { return this.Media.Count == 0; }
        }
    }
}
