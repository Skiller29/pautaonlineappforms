﻿using pautaonlineforms.Helpers;
using System;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
	[XmlRoot(ElementName = "Forum")]
	public class Forum
	{
		[XmlElement(ElementName="ForumMessages")]
		public ForumMessages ForumMessages { get; set; }
		[XmlAttribute(AttributeName="Title")]
		public String Title { get; set; }
		[XmlAttribute(AttributeName="SiteId")]
		public String SiteId { get; set; }
		[XmlAttribute(AttributeName="Subject")]
		public String Subject { get; set; }
		[XmlAttribute(AttributeName="MessagesCounter")]
		public String MessagesCounterNumber { get; set; }

        [XmlIgnore]
        public bool IsVisible { get; set; }

        [XmlAttribute(AttributeName = "isVisible")]
        public string IsVisibleString
        {
            get { return this.IsVisible.ToString(); }
            set
            {
                try
                {
                    this.IsVisible = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.IsVisible = false;
                }
            }
        }

        public String SubjectCompact { get { return Subject.RemoveHtmlTags().RemoveCData().LimitSize(80); } }
    }
}

