﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Institutes")]
    public class Institutes
    {
        [XmlElement(ElementName = "Institute")]
        public List<Institute> Institute;
    }
}
