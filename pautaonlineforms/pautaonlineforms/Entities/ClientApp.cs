﻿namespace pautaonlineforms.Entities
{
    public class ClientApp
    {
        public string AppName { get; set; }
        public string HexaAppColor { get; set; }
        public int IdInstitute { get; set; }
        public string LogoName { get; set; }
    }
}
