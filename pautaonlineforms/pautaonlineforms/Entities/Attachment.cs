﻿using pautaonlineforms.Helpers;
using System;
using System.Threading;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Attachment")]
    public class Attachment
    {
        [XmlElement(ElementName = "Url")]
        public String Url { get; set; }
        [XmlElement(ElementName = "Extension")]
        public String Extension { get; set; }
        [XmlAttribute(AttributeName = "Title")]
        public String Title { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }

        public CancellationToken CancellationToken { get; set; }
        public byte[] FileInBytes { get; set; }
        public String AgendaName { get; set; }
        public String ClassName { get; set; }

        [XmlIgnore]
        public bool IsVisible { get; set; }

        [XmlAttribute(AttributeName = "IsVisible")]
        public string IsVisibleString
        {
            get { return this.IsVisible.ToString(); }
            set
            {
                try
                {
                    this.IsVisible = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.IsVisible = false;
                }
            }
        }

        public String UrlCompact { get { return Url.RemoveHtmlTags().LimitSize(40); } }

        public String Thumbnail
        {
            get
            {
                if (this.Extension == "doc" || this.Extension == "docx")
                    return "file_doc";
                if (this.Extension == "xls" || this.Extension == "xlsx")
                    return "file_xls";
                if (this.Extension == "ppt" || this.Extension == "pptx")
                    return "file_ppt";
                if (this.Extension == "jpg" || this.Extension == "jpeg")
                    return "file_jpeg";
                if (this.Extension == "pdf")
                    return "file_pdf";
                if (this.Extension == "png")
                    return "file_png";
                if (this.Extension == "zip")
                    return "file_zip";
                if (this.Extension == "rar")
                    return "file_rar";
                if (this.Extension == "mp3")
                    return "file_mp3";

                return "pautaonline_default";
            }
        }
    }
}
