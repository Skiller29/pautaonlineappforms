﻿using pautaonlineforms.Helpers;
using System;
using System.Xml.Serialization;


namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Class")]
    public class Class
    {
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }
        [XmlAttribute(AttributeName = "Type")]
        public String Type { get; set; }
        [XmlElement("Coursewares")]
        public Coursewares Coursewares { get; set; }
		[XmlElement(ElementName = "Forums")]
		public Forums Forums { get; set; } 
        [XmlAttribute(AttributeName = "Introduction")]
        public String Introduction { get; set; }
        [XmlAttribute(AttributeName = "CoursewaresCounter")]
        public string CourswaresCounterNumber { get; set; }

        [XmlIgnore]
        public bool IsVisible { get; set; }

        [XmlAttribute(AttributeName = "IsVisible")]
        public string IsVisibleString
        {
            get { return this.IsVisible.ToString(); }
            set
            {
                try
                {
                    this.IsVisible = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.IsVisible = false;
                }
            }
        }

        public String IntroducionFormatted { get { return !string.IsNullOrWhiteSpace(Introduction) ? Introduction.RemoveHtmlTags().LimitSize(40) : ""; } }

        public String AgendaName { get; set; }
        public string PhotoUrl
        {
            get { return "https://apautaon.blob.core.windows.net/images/imagem-padrao-curso.jpg"; }
        }
    }
}
