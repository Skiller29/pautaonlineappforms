﻿using pautaonlineforms.Helpers;
using System;
using System.Threading;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Media")]
    public class Media
    {
        [XmlAttribute(AttributeName = "Title")]
        public String Title { get; set; }
        [XmlAttribute(AttributeName = "SiteId")]
        public String SiteId { get; set; }
        [XmlElement(ElementName = "Url")]
        public String Url { get; set; }
        [XmlElement(ElementName = "Extension")]
        public String Extension { get; set; }
        [XmlElement(ElementName = "Description")]
        public String Description { get; set; }
        public String Thumbnail
        {
            get
            {
                if (this.Extension == "mp4" || this.Url.Contains("vimeo"))
                    return "file_mpeg";
                if (this.Extension == "mp3")
                    return "file_mp3";
                return "pautaonline_default";
            }
        }

        [XmlIgnore]
        public bool IsVisible { get; set; }

        [XmlAttribute(AttributeName = "IsVisible")]
        public string IsVisibleString
        {
            get { return this.IsVisible.ToString(); }
            set
            {
                try
                {
                    this.IsVisible = Boolean.Parse(value);
                }
                catch (Exception)
                {
                    this.IsVisible = false;
                }
            }
        }

        public String DescriptionCompact { get { return Description.RemoveHtmlTags().LimitSize(40); } }

        public String AgendaName { get; set; }
        public String ClassName { get; set; }
        public byte[] FileInBytes { get; set; }
        public CancellationToken CancellationToken { get; set; }
    }
}
