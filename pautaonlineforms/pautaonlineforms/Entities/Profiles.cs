﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Profiles")]
    public class Profiles
    {
        [XmlElement(ElementName = "Profile")]
        public List<Profile> Profile;
    }
}
