﻿using System;

namespace pautaonlineforms.Entities
{
    public class ChatMessage
    {
        public long Id { get; set; }
        public bool IsMe { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
		public string BubbleThumbnail
		{
			get {
				return IsMe ? "bubble_me" : "bubble_other";
			}
		}

        public string DateString
        {
            get { return string.Format("{0:dd/MM/yyyy HH:mm}", this.Date); }
        }

		public string InternalId { get; set; }

        public ChatMessage(bool isMe, string photo, string message)
        {
            this.IsMe = isMe;
            this.Photo = photo;
            this.Message = message;
            this.Date = DateTime.Now;
        }
    }
}
