﻿namespace pautaonlineforms.Entities
{
    public enum ConnectionType
    {
        Cellular,
        WiFi,
        Desktop,
        Wimax,
        Other
    }
}
