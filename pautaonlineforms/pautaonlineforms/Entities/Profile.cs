﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace pautaonlineforms.Entities
{
    [XmlRoot(ElementName = "Profile")]
    public class Profile
    {
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }
        [XmlAttribute(AttributeName = "EnumSite")]
        public String EnumSite { get; set; }
        [XmlIgnore]
        public bool Active { get; set; }

        [XmlAttribute(AttributeName = "Active")]
        public String ActiveString
        {
            get { return this.Active.ToString(); }
            set { this.Active = Boolean.Parse(value); }
        }
    }
}
