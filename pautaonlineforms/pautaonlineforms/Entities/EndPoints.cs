﻿
namespace pautaonlineforms.Entities
{
    public static class EndPoints
    {
        #region Membros
        public const string AuthenticateMemberUrl = "http://apautaon.cloudapp.net/mobile/login";
        public const string LoginAsOtherMemberUrl = "http://apautaon.cloudapp.net/mobile/locarcomooutromembro";
        public const string LogoutMemberUrl = "http://apautaon.cloudapp.net/mobile/logout";
        public const string AuthenticateMemberByFacebookUrl = "http://apautaon.cloudapp.net/mobile/autenticarfacebook";
        public const string EditMemberAccountUrl = "http://apautaon.cloudapp.net/mobile/editarminhaconta";
        public const string EditMemberPasswordUrl = "http://apautaon.cloudapp.net/mobile/editarminhasenha";
        public const string RegisterMemberUrl = "http://apautaon.cloudapp.net/mobile/cadastrar";
        public const string RegisterMemberByFacebookUrl = "http://apautaon.cloudapp.net/mobile/cadastrofacebook";
        public const string ListMemberStructureUrl = "http://apautaon.cloudapp.net/mobile/carregarestrutura";
        public const string ChangeProfileUrl = "http://apautaon.cloudapp.net/mobile/trocarperfil";
        public const string ChangeProfileAuthenticationUrl = "http://apautaon.cloudapp.net/mobile/listarperfisadministradorinstituicao";
        public const string CommunityUrl = "http://apautaon.cloudapp.net/mobile/comunidade";
        public const string ForgetMyPasswordUrl = "http://apautaon.cloudapp.net/mobile/esqueci-minha-senha";
        public const string ResetMyPasswordUrl = "http://apautaon.cloudapp.net/mobile/trocar-minha-senha";
        public const string NumberNotificationUrl = "http://apautaon.cloudapp.net/mobile/numeronotificacoes";
        public const string NotificationsUrl = "http://apautaon.cloudapp.net/mobile/muraldeavisos";
        public const string InformationsMemberUrl = "http://apautaon.cloudapp.net/mobile/carregarinformacoesbasicasmembro";
        public const string InformationsMemberTestUrl = "http://apautaon.cloudapp.net/mobile/carregarinformacoesbasicasmembro";

        #endregion

        #region Biblioteca
        public const string ListContentLibraryUrl = "http://apautaon.cloudapp.net/mobile/listarconteudobiblioteca";
        public const string ListCoursewaresAvailableLibraryUrl = "http://apautaon.cloudapp.net/mobile/listarmateriaisdisponiveisbiblioteca";
        public const string RemoveQuestionLibraryUrl = "http://apautaon.cloudapp.net/mobile/removerquestaobiblioteca";
        public const string ManageCoursewareLibraryUrl = "http://apautaon.cloudapp.net/mobile/gerenciarmaterialdidaticobiblioteca";
        public const string AddCoursewareClassLibraryUrl = "http://apautaon.cloudapp.net/mobile/adicionarmaterialdidaticoaulabiblioteca";
        public const string EditTextCoursewareLibraryUrl = "http://apautaon.cloudapp.net/mobile/editarmaterialdidaticotextobiblioteca";
        public const string EditFileCoursewareLibraryUrl = "http://apautaon.cloudapp.net/mobile/editarmaterialdidaticoarquivobiblioteca";
        public const string AddQuestionLibraryUrl = "http://apautaon.cloudapp.net/mobile/criarquestaobiblioteca";
        public const string EditQuestionLibraryUrl = "http://apautaon.cloudapp.net/mobile/editarquestaobiblioteca";
        #endregion

        #region Cursos
        public const string DetailsAgendaRegistrationUrl = "http://apautaon.cloudapp.net/mobile/detalhespautamatricula";
        public const string DetailsAgendaUrl = "http://apautaon.cloudapp.net/mobile/detalhespauta";
        public const string ListAgendasUrl = "http://apautaon.cloudapp.net/mobile/listarpautas";
        public const string RegisterAgendaUrl = "http://apautaon.cloudapp.net/mobile/matriculapauta";
        public const string ListAgendaCoursewares = "http://apautaon.cloudapp.net/mobile/listarmateriaispauta";
        public const string ListAgendaActivities = "http://apautaon.cloudapp.net/mobile/listaratividadespauta";
        public const string ListAgendaAssessements = "http://apautaon.cloudapp.net/mobile/listaravaliacoespauta";
        public const string DownloadCoursewaresUrl = "http://apautaon.cloudapp.net/mobile/downloadmateriaisdidaticos";
        public const string RegisterMemberAgendaUrl = "http://apautaon.cloudapp.net/mobile/matricularmembropauta";
        public const string ListStatesUrl = "http://apautaon.cloudapp.net/mobile/listarestados";
        public const string ListCitiesUrl = "http://apautaon.cloudapp.net/mobile/listarcidades";
        public const string ManageEnrollmentAgendaUrl = "http://apautaon.cloudapp.net/mobile/gerenciarmatricula";
        public const string ListAllocationsAgendaurl = "http://apautaon.cloudapp.net/mobile/listaralocacoespauta";
        public const string ListImagesCategoryAgendaUrl = "http://apautaon.cloudapp.net/mobile/carregarimagenscategoria";
        public const string LoadInstitutesUrl = "http://apautaon.cloudapp.net/mobile/carregarinstituicoes";
        public const string CreateAgendaUrl = "http://apautaon.cloudapp.net/mobile/criarpauta";
        public const string CreateHierarchyUrl = "http://apautaon.cloudapp.net/mobile/criarhierarquia";
        public const string CreateInstituteUrl = "http://apautaon.cloudapp.net/mobile/criarinstituicaoquick";
        public const string RemoveAgendaUrl = "http://apautaon.cloudapp.net/mobile/excluirpauta";
        public const string EditAgendaUrl = "http://apautaon.cloudapp.net/mobile/editarpauta";
        public const string DuplicateAgendaUrl = "http://apautaon.cloudapp.net/mobile/duplicarpauta";
        public const string ChangeLocalAgendaUrl = "http://apautaon.cloudapp.net/mobile/alterarlocalpauta";
        public const string RegisterAccessAgendaUrl = "http://apautaon.cloudapp.net/mobile/registraracessomembropauta";
        #endregion

        #region Aulas
        public const string DetailsClassUrl = "http://apautaon.cloudapp.net/mobile/gerarxmlaulas";
        public const string RegisterCoursewareViewUrl = "http://apautaon.cloudapp.net/mobile/registrarvisualizacaomaterial";
        public const string CreateClassUrl = "http://apautaon.cloudapp.net/mobile/criaraula";
        public const string EditClassUrl = "http://apautaon.cloudapp.net/mobile/editaraula";
        public const string ManageClassUrl = "http://apautaon.cloudapp.net/mobile/gerenciaraula";
        public const string DoPresenceStudentsClassUrl = "http://apautaon.cloudapp.net/mobile/realizarchamada";
        #endregion

        #region Materiais Didáticos
        public const string AddLinkCoursewareUrl = "http://apautaon.cloudapp.net/mobile/adicionarmaterialdidaticolink";
        public const string FindByCoursewareUrl = "http://apautaon.cloudapp.net/mobile/detalhesmaterialdidatico";
        public const string AddTextCoursewareUrl = "http://apautaon.cloudapp.net/mobile/adicionarmaterialdidaticotexto";
        public const string ChangeClassCoursewareUrl = "http://apautaon.cloudapp.net/mobile/trocaraulamaterialdidatico";
        public const string AddAttachmentCoursewareUrl = "http://apautaon.cloudapp.net/mobile/adicionarmaterialdidaticoanexo";
        public const string AddAudioCoursewareUrl = "http://apautaon.cloudapp.net/mobile/adicionarmaterialdidaticoaudio";
        public const string AddVideoCoursewareUrl = "http://apautaon.cloudapp.net/mobile/adicionarmaterialdidaticovideo";
        public const string EditTextCoursewareUrl = "http://apautaon.cloudapp.net/mobile/editarmaterialdidaticotexto";
        public const string ManageCoursewareUrl = "http://apautaon.cloudapp.net/mobile/gerenciarmaterialdidaticoaula";
        public const string EditMediaCoursewareUrl = "http://apautaon.cloudapp.net/mobile/editarmaterialdidaticomedia";
        #endregion

        #region Fóruns
        public const string DetailsForumUrl = "http://apautaon.cloudapp.net/mobile/listarmensagensforum";
        public const string PostMessageForumUrl = "http://apautaon.cloudapp.net/mobile/postarmensagemforum";
        public const string EditPostMessagemForumUrl = "http://apautaon.cloudapp.net/mobile/editarmensagemforum";
        public const string RemovePostMessagemForumUrl = "http://apautaon.cloudapp.net/mobile/removermensagemforum";
        public const string ReplyMessageForumUrl = "http://apautaon.cloudapp.net/mobile/respondermensagemforum";
        public const string AddForumUrl = "http://apautaon.cloudapp.net/mobile/criarforum";
        public const string EditForumUrl = "http://apautaon.cloudapp.net/mobile/editarforum";
        public const string ManageForumUrl = "http://apautaon.cloudapp.net/mobile/gerenciarforum";
        public const string ManageScoresForumUrl = "http://apautaon.cloudapp.net/mobile/gerenciarnotasforum";
        public const string SaveScoresForumUrl = "http://apautaon.cloudapp.net/mobile/salvarnotasforum";
        public const string SaveScoreSingleForumUrl = "http://apautaon.cloudapp.net/mobile/salvarnotaindividualforum";
        public const string ChangeClassForumUrl = "http://apautaon.cloudapp.net/mobile/trocaraulaforum";
        #endregion

        #region Questionários
        public const string DetailsQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/detalharquestionario";
        public const string StartQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/iniciarquestionario";
        public const string ReplyQuestionUrl = "http://apautaon.cloudapp.net/mobile/responderquestao";
        public const string EndQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/finalizarquestionario";
        public const string ManageQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/gerenciarquestionario";
        public const string ChangeClassQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/trocaraulaquestionario";
        public const string AddQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/criarquestionario";
        public const string EditQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/editarquestionario";

        public const string FindByQuestionUrl = "http://apautaon.cloudapp.net/mobile/detalhesquestao";
        public const string AddQuestionUrl = "http://apautaon.cloudapp.net/mobile/criarquestao";
        public const string EditQuestionUrl = "http://apautaon.cloudapp.net/mobile/editarquestao";
        public const string RemoveQuestionUrl = "http://apautaon.cloudapp.net/mobile/removerquestao";
        public const string ListAllQuestionUrl = "http://apautaon.cloudapp.net/mobile/listarquestoesmembro";
        public const string AddExistQuestionUrl = "http://apautaon.cloudapp.net/mobile/adicionarquestao";
        public const string DetailsAttemptQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/detalhestentativaquestionario";
        public const string SaveCorrectionQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/salvarcorrecaoquestionario";
        public const string SaveObservationCorrectionQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/salvarobservacaocorrecaoquestionario";
        public const string RemoveAttemptQuestionnaireUrl = "http://apautaon.cloudapp.net/mobile/excluirtentativaquestionario";
        #endregion

        #region Avaliações
        public const string DetailsAssessmentUrl = "http://apautaon.cloudapp.net/mobile/detalhesavaliacao";
        public const string MakeAssessmentUrl = "http://apautaon.cloudapp.net/mobile/realizaravaliacao";
        public const string AddAssesmentUrl = "http://apautaon.cloudapp.net/mobile/adicionaravaliacao";
        public const string EditAssesmentUrl = "http://apautaon.cloudapp.net/mobile/editaravaliacao";
        public const string ChangeClassAssessmentUrl = "http://apautaon.cloudapp.net/mobile/trocaraulaavaliacao";
        public const string ManageAssesmentUrl = "http://apautaon.cloudapp.net/mobile/gerenciaravaliacao";
        public const string CorrectAssessmentsUrl = "http://apautaon.cloudapp.net/mobile/corrigiravaliacao";
        public const string CorrectAssessmentSingle = "http://apautaon.cloudapp.net/mobile/corrigiravaliacaoindividual";
        public const string SaveObservationAssessmentUrl = "http://apautaon.cloudapp.net/mobile/salvarobservacaoavaliacao";
        public const string SaveCorrectionAssessmentUrl = "http://apautaon.cloudapp.net/mobile/salvaranexocorrecaoavaliacao";
        public const string RequestReSendAssessmentUrl = "http://apautaon.cloudapp.net/mobile/solicitarreenvioavaliacao";
        public const string HistoryAllocationsAssessmentUrl = "http://apautaon.cloudapp.net/mobile/historicoavaliacoesenviadas";
        #endregion

        #region Mensagens
        public const string SendPrivateMessageUrl = "http://apautaon.cloudapp.net/mobile/enviarmensagem";
        public const string ReplyPrivateMessageUrl = "http://apautaon.cloudapp.net/mobile/respondermensagem";
        public const string ListPrivateMessagesUrl = "http://apautaon.cloudapp.net/mobile/listarmensagens";
        public const string DetailsPrivateMessageUrl = "http://apautaon.cloudapp.net/mobile/detalhesmensagem";
        public const string ListOldChatMessagesUrl = "http://apautaon.cloudapp.net/mobile/listarmensagensantigaschat";
        public const string SendNotificationUrl = "http://apautaon.cloudapp.net/mobile/enviarnotificacao";
        #endregion

        #region Wiki, Glossário e Bate-papo
        public const string DetailsWikiUrl = "http://apautaon.cloudapp.net/mobile/detalheswiki";
        public const string EditWikiUrl = "http://apautaon.cloudapp.net/mobile/editarwiki";
        public const string ManageWikiUrl = "http://apautaon.cloudapp.net/mobile/gerenciarwiki";

        public const string DetailsGlossaryUrl = "http://apautaon.cloudapp.net/mobile/detalhesglossario";
        public const string AddGlossaryEntryUrl = "http://apautaon.cloudapp.net/mobile/adicionaritemglossario";
        public const string EditGlossaryEntryUrl = "http://apautaon.cloudapp.net/mobile/editaritemglossario";
        public const string RemoveGlossaryEntryUrl = "http://apautaon.cloudapp.net/mobile/excluiritemglossario";
        public const string ManageGlossaryUrl = "http://apautaon.cloudapp.net/mobile/gerenciarglossario";

        public const string ManageChatUrl = "http://apautaon.cloudapp.net/mobile/gerenciarbatepapo";
        #endregion

        #region Componentes
        public const string ListInstitutesUrl = "http://apautaon.cloudapp.net/mobile/listarinstituicoes";
        public const string ListHierarchiesUrl = "http://apautaon.cloudapp.net/mobile/listarhierarquiasinstituicao";
        public const string FilterCitiesUrl = "http://apautaon.cloudapp.net/mobile/filtrocidades";
        public const string ControlDeviceUrl = "http://apautaon.cloudapp.net/mobile/controlardispositivo";
        #endregion

        #region Relatórios
        public const string ReportAccessMemberAgendaUrl = "http://apautaon.cloudapp.net/mobile/relatorioacessospautamembro";
        public const string ReportAccessAllMembersAgendaUrl = "http://apautaon.cloudapp.net/mobile/relatorioacessotodosmembrospauta";
        public const string ReportAccessMemberUrl = "http://apautaon.cloudapp.net/mobile/relatorioacessosmembro";
        public const string ListProfilesMembersAgendaUrl = "http://apautaon.cloudapp.net/mobile/retornarperfismatriculadospauta";
        public const string ListMembersAgendaUrl = "http://apautaon.cloudapp.net/mobile/listarmembrospauta";
        public const string ReportStudentsUrl = "http://apautaon.cloudapp.net/mobile/relatorioalunos";
        public const string ReportAssesmentsUrl = "http://apautaon.cloudapp.net/mobile/relatorioavaliacao";
        public const string ReportForumUrl = "http://apautaon.cloudapp.net/mobile/relatorioforum";
        public const string ReportFrequencyUrl = "http://apautaon.cloudapp.net/mobile/relatoriofrequencia";
        public const string ReportChatUrl = "http://apautaon.cloudapp.net/mobile/relatoriochataluno";
        public const string ReportCoursewaresUrl = "http://apautaon.cloudapp.net/mobile/relatoriomateriaisdidaticos";
        public const string ReportNotesUrl = "http://apautaon.cloudapp.net/mobile/relatorionotas";
        public const string ReportDetailsMemberUrl = "http://apautaon.cloudapp.net/mobile/detalhesmembro";
        public const string ListActivitiesWithScoreUrl = "http://apautaon.cloudapp.net/mobile/listaratividadescomnota";

        public const string ListDoneAssessmentsStudentUrl = "http://apautaon.cloudapp.net/mobile/listaravaliacoesconcluidasaluno";
        public const string ListAccessCoursewaresStudentUrl = "http://apautaon.cloudapp.net/mobile/listarmateriaisacessadosaluno";
        public const string ListAccessForumsStudentUrl = "http://apautaon.cloudapp.net/mobile/listarforunsacessadosaluno";
        public const string ListChatMessagesStudentUrl = "http://apautaon.cloudapp.net/mobile/listarmensagenschataluno";
        #endregion

        #region Funcionalidades Coordenador
        public const string ListHierachiesCoordinatorUrl = "http://apautaon.cloudapp.net/mobile/listarhierarquiascoordenador";
        #endregion

        #region Funcionalidades Admin Instituição
        public const string SignAsOtherMemberUrl = "http://apautaon.cloudapp.net/mobile/logarcomooutromembro";
        public const string ChangePasswordMemberUrl = "http://apautaon.cloudapp.net/mobile/alterarsenhamembro";
        public const string ChangeEmailMemberUrl = "http://apautaon.cloudapp.net/mobile/alteraremailmembro";
        public const string RemoveAllocationMemberUrl = "http://apautaon.cloudapp.net/mobile/removeralocacaomembro";
        public const string EnrollmentListStudentsUrl = "http://apautaon.cloudapp.net/mobile/matricularlistaalunos";
        public const string ReportNewAgendasUrl = "http://apautaon.cloudapp.net/mobile/relatoriopautascriadas";
        public const string ListAgendasHierarchy = "http://apautaon.cloudapp.net/mobile/listarpautashierarquia";
        public const string ListInteractionsTeachersUrl = "http://apautaon.cloudapp.net/mobile/listarinteracaoprofessores";
        public const string CreateNewMemberUrl = "http://apautaon.cloudapp.net/mobile/cadastrarmembroinstituicao";
        public const string InsertProfileMemberUrl = "http://apautaon.cloudapp.net/mobile/inserirperfilmembro";
        public const string ListHierachiesInstituteUrl = "http://apautaon.cloudapp.net/mobile/listarhierarquiasinstituicaoadmin";
        public const string EditHierarchyUrl = "http://apautaon.cloudapp.net/mobile/editarhierarquia";
        public const string ChangePlaceHierarchyUrl = "http://apautaon.cloudapp.net/mobile/alterarlocalhierarquia";
        public const string RemoveHierarchyUrl = "http://apautaon.cloudapp.net/mobile/excluirhierarquia";
        public const string InsertPlanMemberUrl = "http://apautaon.cloudapp.net/mobile/atribuirplano";
        public const string ChangeProfileMemberUrl = "http://apautaon.cloudapp.net/mobile/alterarperfilmembro";
        public const string ListCoordinatorsHierarchyUrl = "http://apautaon.cloudapp.net/mobile/listarcoordenadoreshierarquia";
        public const string AddCoordinatorHierarchyUrl = "http://apautaon.cloudapp.net/mobile/inserircoordenadorhierarquia";
        public const string RemoveCoordinatorHierarchyUrl = "http://apautaon.cloudapp.net/mobile/removercoordenadorhierarquia";

        #endregion

    }
}
