﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class CityService : BaseService
    {
        public static Task<States> ListarEstados()
        {
            var url = $"{EndPoints.ListStatesUrl}?idioma=pt&chave={Settings.ActivationToken}";
            return new RestService().MakeGetRequestHttpClient<States>(url);
        }

        public static Task<Cities> ListarCidades(string state)
        {
            var url = $"{EndPoints.ListCitiesUrl}?idioma=pt&chave={Settings.ActivationToken}&estado={state}";
            return new RestService().MakeGetRequestHttpClient<Cities>(url);
        }
    }
}
