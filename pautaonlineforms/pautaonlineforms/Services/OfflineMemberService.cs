﻿using pautaonlineforms.Database;
using pautaonlineforms.Entities;
using System;
using System.Linq.Expressions;

namespace pautaonlineforms.Services
{
    public class OfflineMemberService
    {
        public static void SaveOrUpdate(OfflineMember offlineMember)
        {
            var check = Find(x => x.ActivationToken == offlineMember.ActivationToken);
            if (check != null)
            {
                Update(check, offlineMember);
            }
            else
            {
                using (var dados = new DataAccess<OfflineMember>())
                {
                    dados.Save(offlineMember);
                }
            }
        }

        public static OfflineMember Find(Expression<Func<OfflineMember, bool>> predicate)
        {
            using (var dados = new DataAccess<OfflineMember>())
            {
                return dados.Find(predicate);
            }            
        }

        public static bool CheckIfExists(OfflineMember offlineMember)
        {
            var check = Find(x => x.ActivationToken == offlineMember.ActivationToken);
            return check != null;
        }

        public static OfflineMember Authenticate(string email, string senha)
        {
            using (var dados = new DataAccess<OfflineMember>())
            {
                var users = dados.ListAll();
            }

            return Find(x => x.Email == email && x.Password == senha);
        }

        public static void Update(OfflineMember originalOfflineMember, OfflineMember editedOfflineMember)
        {
            originalOfflineMember.ActiveProfile = editedOfflineMember.ActiveProfile;
            originalOfflineMember.FullName = editedOfflineMember.FullName;
            originalOfflineMember.Password = editedOfflineMember.Password;
            using (var dados = new DataAccess<OfflineMember>())
            {
                dados.Update(originalOfflineMember);
            }            
        }
    }
}
