﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class MemberService : BaseService
    {
        public static Task<Member> Authenticate(string email, string password)
        {
            var url = $"{EndPoints.AuthenticateMemberUrl}?idioma=pt&email={email}&senha={password}&idInstituicao={Settings.IdInstitute}";
            return new RestService().MakePostRequestHttpClient<Member>(url);
        }

        public static Task<Member> ListStructure()
        {
            var url = $"{EndPoints.ListMemberStructureUrl}?idioma=pt&chave={Settings.ActivationToken}&idInstituicao={Settings.IdInstitute}";
            return new RestService().MakeGetRequestHttpClient<Member>(url);
        }

        public static Task<Member> AuthenticateByFacebook(string facebookId)
        {
            var url = $"{EndPoints.AuthenticateMemberByFacebookUrl}?idioma=pt&facebookid={facebookId}";
            return new RestService().MakePostRequestHttpClient<Member>(url);
        }

        public static Task<Member> UpdateAccount(string nome, DateTime dataNascimento, string cep, string celular, string endereco, string numero, string bairro, string complemento, string skype, string estado, string cidade, string perfil)
        {
            var url = $"{EndPoints.EditMemberAccountUrl}?idioma=pt&chave={Settings.ActivationToken}&fullname={nome}&celular={celular}&cep={cep}&street={endereco}&number={numero}&complement={complemento}&neighborhood={bairro}&state={estado}&city={cidade}&skype={skype}&perfilAtivo={perfil}&dataNascimento={(dataNascimento != DateTime.MinValue ? dataNascimento.ToString("yyyy-MM-dd") : "")}";
            return new RestService().MakePostRequestHttpClient<Member>(url);
        }

        public static void Logout()
        {
            Settings.ActivationToken = String.Empty;
            Settings.UrlImage = String.Empty;
            Settings.Profile = String.Empty;
            Settings.Email = String.Empty;
            Settings.FullName = String.Empty;
        }

        public static Task<Member> ControlDevice(string os, string token)
        {
            var url = $"{EndPoints.ControlDeviceUrl}?idioma=pt&chave={Settings.ActivationToken}&os={os}&dispositivo={token}";
            return new RestService().MakePostRequestHttpClient<Member>(url);
        }

        public static Task<Member> FindByActivationToken()
        {
            var url = string.Format("{0}?idioma=pt&chave={1}", EndPoints.EditMemberAccountUrl, Settings.ActivationToken);
            return new RestService().MakeGetRequestHttpClient<Member>(url);
        }
    }
}
