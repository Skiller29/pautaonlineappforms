﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class ClassService : BaseService
    {
        public static Task<Class> FindByIdAndActivationToken(string id)
        {
            var url = $"{EndPoints.DetailsClassUrl}?idioma=pt&chave={Settings.ActivationToken}&idaula={id}";
            return new RestService().MakeGetRequestHttpClient<Class>(url);
        }
    }
}
