﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class RestService
    {
        public string Response { get; set; }

        public async Task<T> MakeGetRequestHttpClient<T>(string url)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetStringAsync(url);
                    return Deserialize<T>(response);
                }
                catch (Exception ex)
                {

                    throw;
                }                              
            }
        }

        public async Task<T> MakePostRequestHttpClient<T>(string url, Dictionary<string, string> parameters = null)
        {
            using (var client = new HttpClient())
            {
                var builder = new StringBuilder();
                HttpResponseMessage response = null;
                if (parameters == null || !parameters.Any())
                {
                    response = await client.PostAsync(url, new StringContent("", Encoding.UTF8));
                }
                else
                {
                    MultipartFormDataContent formData = new MultipartFormDataContent();
                    foreach (var item in parameters)
                    {
                        formData.Add(new StringContent(item.Value), item.Key);
                    }
                    response = await client.PostAsync(url, formData);
                }


                return Deserialize<T>(response.Content.ReadAsStringAsync().Result);
            }
        }

        public async Task<T> MakePostRequestFile<T>(string url, string fileName, string contentType, MemoryStream stream)
        {
            using (var client = new HttpClient())
            {
                var form = new MultipartFormDataContent();
                var attachment = new ByteArrayContent(stream.ToArray(), 0, stream.ToArray().Length);
                attachment.Headers.ContentType = new MediaTypeHeaderValue(contentType);
                form.Add(attachment, contentType.Split('/')[0], fileName);

                var response = await client.PostAsync(url, form);
                return Deserialize<T>(response.Content.ReadAsStringAsync().Result);
            }
        }

        private T Deserialize<T>(string response)
        {
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            var toReturn = (T)serializer.Deserialize(GenerateStreamFromString(response));
            return toReturn;
        }

         public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}
