﻿using pautaonlineforms.Database;
using pautaonlineforms.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Internals;

namespace pautaonlineforms.Services
{
    public class OfflineCoursewareService
    {
        public static void Save(OfflineCourseware offlineCourseware)
        {
            if (CheckIfExists(offlineCourseware) == false)
            {
                using (var dados = new DataAccess<OfflineCourseware>())
                {
                    dados.Save(offlineCourseware);
                }
            }                
        }

        public static OfflineCourseware Find(Expression<Func<OfflineCourseware, bool>> predicate)
        {
            using (var dados = new DataAccess<OfflineCourseware>())
            {
                return dados.Find(predicate);
            }
        }

        public static bool CheckIfExists(OfflineCourseware offlineCourseware)
        {
            var check = Find(x => x.MemberActivationToken == offlineCourseware.MemberActivationToken && x.SiteId == offlineCourseware.SiteId);
            return check != null;
        }

        public static void Delete(OfflineCourseware offlineCourseware)
        {
            using (var dados = new DataAccess<OfflineCourseware>())
            {
                dados.Delete(offlineCourseware.Id);
            }
        }

        public static List<OfflineCourseware> ListAll(string activationToken)
        {
            using (var dados = new DataAccess<OfflineCourseware>())
            {
                return dados.ListAll().Where(x => x.MemberActivationToken == activationToken).ToList();
            }
        }

    }
}
