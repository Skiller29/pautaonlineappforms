﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class ForumService : BaseService
    {
        public static Task<Forum> FindByIdAndActivationToken(string id)
        {
            var url = $"{EndPoints.DetailsForumUrl}?idioma=pt&chave={Settings.ActivationToken}&idforum={id}";
            return new RestService().MakeGetRequestHttpClient<Forum>(url);
        }

        public static Task<Forum> Post(string id, string message)
        {
            var url = $"{EndPoints.PostMessageForumUrl}?idioma=pt&chave={Settings.ActivationToken}&idforum={id}&mensagem={message}";
            return new RestService().MakePostRequestHttpClient<Forum>(url);
        }

        public static Task<Forum> ReplyPost(string id, string message, string idMessage)
        {
            var url = $"{EndPoints.ReplyMessageForumUrl}?idioma=pt&chave={Settings.ActivationToken}&idforum={id}&mensagem={message}&idmensagempai={idMessage}";
            return new RestService().MakePostRequestHttpClient<Forum>(url);
        }
    }
}
