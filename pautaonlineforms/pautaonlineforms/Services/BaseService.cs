﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace pautaonlineforms.Services
{
    public abstract class BaseService
    {
        protected RestService RestService { get; set; }

        protected BaseService()
        {
            RestService = new RestService();
        }
    }
}
