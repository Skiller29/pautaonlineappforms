﻿using pautaonlineforms.Database;
using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class NotificationsService
    {
        public static void Save(Notification notification)
        {
            using (var dados = new DataAccess<Notification>())
            {
                dados.Save(notification);
            }
        }

        public static List<Notification> ListAll(string activationToken)
        {
            using (var dados = new DataAccess<Notification>())
            {
                return dados.ListAll().Where(x=>x.MemberActivationToken == activationToken).ToList();
            }
        }

        public static void Delete(Notification notification)
        {
            using (var dados = new DataAccess<Notification>())
            {
                dados.Delete(notification.Id);
            }
        }

        public static Notification Find(Expression<Func<Notification, bool>> predicate)
        {
            using (var dados = new DataAccess<Notification>())
            {
                return dados.Find(predicate);
            }
        }

        public static Task<Notification> Send(string idAgenda, string message)
        {
            var url = $"{EndPoints.SendNotificationUrl}?idioma=pt&chave={Settings.ActivationToken}&idpauta={idAgenda}&mensagem={message}";
            return new RestService().MakePostRequestHttpClient<Notification>(url);
        }
    }
}
