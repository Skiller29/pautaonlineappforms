﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.Services
{
    public class AgendasService : BaseService
    {
        public static Task<Agenda> FindByIdAndActivationToken(string id)
        {
            var url = $"{EndPoints.DetailsAgendaUrl}?idioma=pt&chave={Settings.ActivationToken}&idpauta={id}";
            return new RestService().MakeGetRequestHttpClient<Agenda>(url);
        }
    }
}
