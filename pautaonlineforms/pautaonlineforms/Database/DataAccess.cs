﻿using pautaonlineforms.Entities;
using pautaonlineforms.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pautaonlineforms.Database
{
    public class DataAccess<T> : IDisposable where T : class
    {
        private SQLite.Net.SQLiteConnection _sqLiteConnection;

        public DataAccess()
        {
            var config = DependencyService.Get<IRepository>();
            _sqLiteConnection = new SQLite.Net.SQLiteConnection(config.Plataforma, System.IO.Path.Combine(config.DiretorioDB, "pautaonline.db3"));

            _sqLiteConnection.CreateTable<OfflineMember>();
            _sqLiteConnection.CreateTable<OfflineCourseware>();
            _sqLiteConnection.CreateTable<Notification>();
        }

        public void Save(T item)
        {
            _sqLiteConnection.Insert(item);
        }

        public List<T> ListAll()
        {
            return _sqLiteConnection.Table<T>().ToList();
        }

        public void DeleteAll()
        {
            _sqLiteConnection.DeleteAll<T>();
        }

        public T Find(Expression<Func<T, bool>> predicate)
        {
            return _sqLiteConnection.Find<T>(predicate);
        }

        public void Delete(Expression<Func<T, bool>> predicate)
        {
            var toDelete = Find(predicate);
            if (toDelete != null)
                _sqLiteConnection.Delete(toDelete);
        }

        public void Delete(int id)
        {
            _sqLiteConnection.Delete<T>(id);
        }

        public void Update(T item)
        {
            _sqLiteConnection.Update(item);
        }

        public void Dispose()
        {
            _sqLiteConnection.Dispose();
        }
    }
}
