﻿using System;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;

namespace pautaonlineforms.Helpers
{
    public static class StringHelper
    {
        public static string RemoveHtmlTags(this string str)
        {
            return Regex.Replace(str, "<.*?>", string.Empty);
        }

        public static string RemoveNewLineCharacters(this string str)
        {
            return Regex.Replace(str, @"\t|\n|\r", string.Empty);
        }

        public static string LimitSize(this string str, int size)
        {
            if (string.IsNullOrWhiteSpace(str)) return string.Empty;

            str = Regex.Replace(str, @"<[^>]*>", String.Empty);

            if ((size - 4) <= 0) return str;
            if (str.Length <= size - 4) return str;

            return string.Format("{0}...", str.Substring(0, size - 4));
        }

		public static string FriendlyName(this string str)
		{
			if (string.IsNullOrWhiteSpace(str)) return "";
            return Regex.Replace(str, @"[^A-Za-z0-9_\.~]+", "-");
		}

        public static string PlainText(this string str, int size)
        {
            return Regex.Replace(str.RemoveHtmlTags().RemoveNewLineCharacters().LimitSize(size), @"\s*(<[^>]+>)\s*", "$1", RegexOptions.Singleline);
        }

		public static string TrimStart(this string target, string trimString)
		{
			string result = target;
			while (result.StartsWith(trimString))
			{
				result = result.Substring(trimString.Length);
			}

			return result;
		}

		public static string TrimEnd(this string target, string trimString)
		{
			string result = target;
			while (result.EndsWith(trimString))
			{
				result = result.Substring(0, result.Length - trimString.Length);
			}

			return result;
		}

        public static string RemoveCData(this string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return "";
            return str.Trim().Replace("<![CDATA[", "").Replace("]]>", "").Trim();
        }

        public static string FormatAccent(this string str)
        {
            if (string.IsNullOrWhiteSpace(str)) return "";

            byte[] utf8bytes = Encoding.UTF8.GetBytes(str);
            string r = Encoding.UTF8.GetString(utf8bytes, 0, utf8bytes.Length);
            return r;
        }
    }
}
