// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace pautaonlineforms.Helpers
{
	/// <summary>
	/// This is the Settings static class that can be used in your Core solution or in any
	/// of your client applications. All settings are laid out the same exact way with getters
	/// and setters. 
	/// </summary>
	public static class Settings
	{
		private static ISettings AppSettings
		{
			get
			{
				return CrossSettings.Current;
			}
		}

		#region Setting Constants

		private const string SettingsKey = "settings_key";
		private static readonly string SettingsDefault = string.Empty;

        private const string ActivationTokenKey = "ActivationToken";
        private static readonly string ActivationTokenDefault = string.Empty;

        private const string FullNameKey = "FullName";
        private static readonly string FullNameDefault = string.Empty;

        private const string ProfileKey = "Profile";
        private static readonly string ProfileDefault = string.Empty;

        private const string UrlImageKey = "UrlImage";
        private static readonly string UrlImageDefault = string.Empty;

        private const string EmailKey = "Email";
        private static readonly string EmailDefault = string.Empty;



        //CONFIGURAÇÕES CUSTOMIZADAS DO APP
        private const string ProjectNameKey = "ProjectName";
        private static readonly string ProjectNameDefault = string.Empty;

        private const string PrimaryColorKey = "PrimaryColor";
        private static readonly string PrimaryColorDefault = string.Empty;

        private const string IdInstituteKey = "IdInstitute";
        private static readonly string IdInstituteDefault = string.Empty;

        private const string LogoNameKey = "LogoName";
        private static readonly string LogoNameDefault = string.Empty;
        #endregion


        public static string GeneralSettings
		{
			get
			{
				return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
			}
			set
			{
				AppSettings.AddOrUpdateValue(SettingsKey, value);
			}
		}

        public static string ActivationToken
        {
            get
            {
                return AppSettings.GetValueOrDefault(ActivationTokenKey, ActivationTokenDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ActivationTokenKey, value);
            }
        }

        public static string FullName
        {
            get
            {
                return AppSettings.GetValueOrDefault(FullNameKey, FullNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(FullNameKey, value);
            }
        }

        public static string Profile
        {
            get
            {
                return AppSettings.GetValueOrDefault(ProfileKey, ProfileDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ProfileKey, value);
            }
        }

        public static string UrlImage
        {
            get
            {
                return AppSettings.GetValueOrDefault(UrlImageKey, UrlImageDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(UrlImageKey, value);
            }
        }

        public static string Email
        {
            get
            {
                return AppSettings.GetValueOrDefault(EmailKey, EmailDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(EmailKey, value);
            }
        }


        //CONFIGURAÇÕES CUSTOMIZADAS DO APP
        public static string ProjectName
        {
            get
            {
                return AppSettings.GetValueOrDefault(ProjectNameKey, ProjectNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ProjectNameKey, value);
            }
        }

        public static string PrimaryColor
        {
            get
            {
                return AppSettings.GetValueOrDefault(PrimaryColorKey, PrimaryColorDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PrimaryColorKey, value);
            }
        }

        public static string IdInstitute
        {
            get
            {
                return AppSettings.GetValueOrDefault(IdInstituteKey, IdInstituteDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IdInstituteKey, value);
            }
        }

        public static string LogoName
        {
            get
            {
                return AppSettings.GetValueOrDefault(LogoNameKey, LogoNameDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(LogoNameKey, value);
            }
        }
    }
}