﻿
using Acr.UserDialogs;
using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Interfaces;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using Plugin.Connectivity;
using PushNotification.Plugin;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pautaonlineforms
{
    public partial class App : Application
    {

        public bool Connected { get; set; } = true;

        public App()
        {
            DependencyService.Register<ViewModels.Services.IMessageService, Views.Services.MessageService>();
            DependencyService.Register<IClientApp>();
            DependencyService.Register<IOfflineCourseware>();
            InitializeComponent();
            GetInformationsApp();
            InitApp();
        }

        public void InitApp()
        {
            if (string.IsNullOrWhiteSpace(Settings.ActivationToken))
            {
                var navigation = new NavigationPage(new IntroductionPage());
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                        break;
                    case Device.Android:
                        navigation.BarTextColor = Color.White;
                        break;
                    default:
                        navigation.BarTextColor = Color.White;
                        break;
                }
                this.MainPage = navigation;
            }
            else
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    Connected = false;
                    var navigation = new NavigationPage(new MyCoursewaresPage());
                    navigation.Icon = "hamburger";
                    navigation.Title = Settings.ProjectName;
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                            break;
                        case Device.Android:
                            navigation.BarTextColor = Color.White;
                            break;
                        default:
                            navigation.BarTextColor = Color.White;
                            break;
                    }
                    var index = new IndexPage()
                    {
                        Master = new Views.MasterPage(false) { Title = Settings.ProjectName },
                        Detail = navigation,
                        IsPresented = false
                    };
                    App.Current.MainPage = index;
                }
                else
                {
                    var navigation = new NavigationPage(new MyAgendasPage());
                    navigation.Icon = "hamburger";
                    navigation.Title = Settings.ProjectName;
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                            break;
                        case Device.Android:
                            navigation.BarTextColor = Color.White;
                            break;
                        default:
                            navigation.BarTextColor = Color.White;
                            break;
                    }
                    var index = new IndexPage()
                    {
                        Master = new Views.MasterPage() { Title = Settings.ProjectName},
                        Detail = navigation,
                        IsPresented = false
                    };
                    App.Current.MainPage = index;
                }                
            }
        }

        public void GetInformationsApp()
        {
            var clientApp = DependencyService.Get<IClientApp>().GetClient();
            if(string.IsNullOrWhiteSpace(Settings.PrimaryColor) || string.IsNullOrWhiteSpace(Settings.ProjectName) || string.IsNullOrWhiteSpace(Settings.LogoName))
            {
                Settings.PrimaryColor = clientApp.HexaAppColor;
                Settings.ProjectName = clientApp.AppName;
                Settings.LogoName = clientApp.LogoName;
                Settings.IdInstitute = clientApp.IdInstitute == 0 ? "" : clientApp.IdInstitute.ToString();
            }
        }

        public async static void GetToken(string token, string os)
        {
            if (!string.IsNullOrWhiteSpace(Settings.ActivationToken))
            {
                await MemberService.ControlDevice(os, token);
            }
        }

        public static void SaveNotification(Notification notification)
        {
            NotificationsService.Save(notification);
        }

        public static Action HideLoginView
        {
            get
            {
                return new Action(() => App.Current.MainPage.Navigation.PopAsync());
            }
        }

        public async static Task AuthenticationFacebook(string appId, string nome, string email, string foto)
        {
            UserDialogs.Instance.ShowLoading("Carregando", MaskType.Clear);
            var membro = await MemberService.AuthenticateByFacebook(appId);
            if (!string.IsNullOrWhiteSpace(membro.Error))
            {
                NavigationPage navigation = new NavigationPage();
                await navigation.PushAsync(new LoginPage());
                navigation.Title = "Pauta Online";
                App.Current.MainPage = navigation;
                UserDialogs.Instance.Toast(membro.Error, new TimeSpan(0, 0, 4));
            }
            else
            {
                Settings.ActivationToken = membro.ActivationToken;
                Settings.FullName = membro.FullName;
                Settings.Profile = membro.GetActiveProfile().Name;
                Settings.UrlImage = membro.PhotoUrl;
                Settings.Email = membro.Email;

                var navigation = new NavigationPage(new MyAgendasPage());
                navigation.Icon = "hamburger";
                navigation.Title = Settings.ProjectName;
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                        break;
                    case Device.Android:
                        navigation.BarTextColor = Color.White;
                        break;
                    default:
                        navigation.BarTextColor = Color.White;
                        break;
                }

                var index = new IndexPage()
                {
                    Master = new Views.MasterPage() { Title = Settings.ProjectName },
                    Detail = navigation,
                    IsPresented = false
                };
                App.Current.MainPage = index;
            }
            UserDialogs.Instance.HideLoading();
        }

        protected override void OnStart()
        {
            CrossPushNotification.Current.Register();
        }

        protected override void OnSleep()
        {

        }

        protected override void OnResume()
        {
            if(!string.IsNullOrWhiteSpace(Settings.ActivationToken))
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    Connected = false;
                    var navigation = new NavigationPage(new MyCoursewaresPage());
                    navigation.Icon = "hamburger";
                    navigation.Title = Settings.ProjectName;
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                            break;
                        case Device.Android:
                            navigation.BarTextColor = Color.White;
                            break;
                        default:
                            navigation.BarTextColor = Color.White;
                            break;
                    }
                    var index = new IndexPage()
                    {
                        Master = new Views.MasterPage(false) { Title = Settings.ProjectName },
                        Detail = navigation,
                        IsPresented = false
                    };
                    App.Current.MainPage = index;
                }
                else
                {
                    if (!Connected)
                    {
                        InitApp();
                    }
                }
            }           
        }
    }
}
