﻿using System.IO;
using System.Threading.Tasks;

namespace pautaonlineforms.Interfaces
{
    public interface IOfflineCourseware
    {
        string Save(string filename, string email, string url);
        string Load(string filename);
        void Delete(string filename);
        bool FileExists(string filename);
        Stream GetStreamPDF(string url);
    }
}
