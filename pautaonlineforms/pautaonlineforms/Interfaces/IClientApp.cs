﻿using pautaonlineforms.Entities;

namespace pautaonlineforms.Interfaces
{
    public interface IClientApp
    {
        ClientApp GetClient();
    }
}
