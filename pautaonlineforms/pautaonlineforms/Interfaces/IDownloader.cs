﻿using System.IO;


namespace pautaonlineforms.Interfaces
{
    public interface IDownloader
    {
        Stream DownloadPdfStream(string URL, string documentName);
    }
}
