﻿using Acr.UserDialogs;
using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Interfaces;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class MyClassViewModel : BaseViewModel
    {
        private readonly IOfflineCourseware _offlineCourseware;

        private List<Text> _textos;
        public List<Text> Textos
        {
            get { return _textos; }
            set
            {
                _textos = value;
                NotifyPropertyChanged("Textos");
            }
        }

        private Text _texto;
        public Text Texto
        {
            get { return _texto; }
            set
            {
                _texto = value;
                NotifyPropertyChanged("Texto");
                if (Texto != null)
                {
                    OpcoesTexto(_texto);
                    Texto = null;
                }
            }
        }

        private List<Link> _links;
        public List<Link> Links
        {
            get { return _links; }
            set
            {
                _links = value;
                NotifyPropertyChanged("Links");
            }
        }

        private Link _link;
        public Link Link
        {
            get { return _link; }
            set
            {
                _link = value;
                NotifyPropertyChanged("Link");
                if (Link != null)
                {
                    OpcoesLink(_link);
                    Link = null;
                }
            }
        }

        private List<Attachment> _anexos;
        public List<Attachment> Anexos
        {
            get { return _anexos; }
            set
            {
                _anexos = value;
                NotifyPropertyChanged("Anexos");
            }
        }

        private Attachment _anexo;
        public Attachment Anexo
        {
            get { return _anexo; }
            set
            {
                _anexo = value;
                NotifyPropertyChanged("Anexo");
                if (Anexo != null)
                {
                    OpcoesAnexo(_anexo);
                    Anexo = null;
                }
            }
        }

        private List<Media> _midias;
        public List<Media> Midias
        {
            get { return _midias; }
            set
            {
                _midias = value;
                NotifyPropertyChanged("Midias");
            }
        }

        private Media _midia;
        public Media Midia
        {
            get { return _midia; }
            set
            {
                _midia = value;
                NotifyPropertyChanged("Midia");
                if (Midia != null)
                {
                    OpcoesMidia(_midia);
                    Midia = null;
                }
            }
        }

        private List<Forum> _foruns;
        public List<Forum> Foruns
        {
            get { return _foruns; }
            set
            {
                _foruns = value;
                NotifyPropertyChanged("Foruns");
            }
        }

        private Forum _forum;
        public Forum Forum
        {
            get { return _forum; }
            set
            {
                _forum = value;
                NotifyPropertyChanged("Forum");
                if (Forum != null)
                {
                    DetalhesForum(_forum);
                    Forum = null;
                }
            }
        }

        private string _titulo;
        public string Titulo
        {
            get { return _titulo; }
            set
            {
                _titulo = value;
                NotifyPropertyChanged("Titulo");                
            }
        }

        public string NomePauta { get; set; }

        public MyClassViewModel(string idAula, string nomePauta)
        {
            CarregarAula(idAula);
            NomePauta = nomePauta;
            _offlineCourseware = DependencyService.Get<IOfflineCourseware>();
        }

        private async void CarregarAula(string idAula)
        {
            IsBusy = true;
            var aula = await ClassService.FindByIdAndActivationToken(idAula);
            Titulo = aula.Name;
            if (Settings.Profile == "Aluno")
            {
                Foruns = aula.Forums != null && aula.Forums.Forum.Any() ? aula.Forums.Forum.Where(x=>x.IsVisible).ToList() : new List<Forum>();
                Textos = aula.Coursewares.Text != null && aula.Coursewares.Text.Any() ? aula.Coursewares.Text.Where(x => x.IsVisible).ToList() : new List<Text>(); ;
                Links = aula.Coursewares.Link != null && aula.Coursewares.Link.Any() ? aula.Coursewares.Link.Where(x => x.IsVisible).ToList() : new List<Link>();
                Anexos = aula.Coursewares.Attachment != null && aula.Coursewares.Attachment.Any() ? aula.Coursewares.Attachment.Where(x => x.IsVisible).ToList() : new List<Attachment>();
                Midias = aula.Coursewares.Media != null && aula.Coursewares.Media.Any() ? aula.Coursewares.Media.Where(x => x.IsVisible).ToList() : new List<Media>();
            }
            else
            {
                Foruns = aula.Forums != null && aula.Forums.Forum.Any() ? aula.Forums.Forum : new List<Forum>();
                Textos = aula.Coursewares.Text != null && aula.Coursewares.Text.Any() ? aula.Coursewares.Text : new List<Text>(); ;
                Links = aula.Coursewares.Link != null && aula.Coursewares.Link.Any() ? aula.Coursewares.Link : new List<Link>();
                Anexos = aula.Coursewares.Attachment != null && aula.Coursewares.Attachment.Any() ? aula.Coursewares.Attachment : new List<Attachment>();
                Midias = aula.Coursewares.Media != null && aula.Coursewares.Media.Any() ? aula.Coursewares.Media : new List<Media>();
            }            
            IsBusy = false;
        }

        private async void OpcoesTexto(Text texto)
        {
            string opcao = "";
            if(OfflineCoursewareService.Find(x=>x.Title == texto.Title) == null)
                opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com o texto?", "Cancelar", "", null, "Abrir", "Salvar");
            else
                opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com o texto?", "Cancelar", "", null, "Abrir");

            switch (opcao)
            {
                case "Abrir":
                    await MD.Navigation.PushAsync(new PreviewTextPage(texto.Title, texto.Description));
                    break;
                case "Salvar":
                    OfflineCoursewareService.Save(new OfflineCourseware(texto, Settings.ActivationToken, NomePauta));
                    ShowMessage("Texto salvo com sucesso");
                    break;
                default:
                    break;
            }
        }

        private async void OpcoesMidia(Media midia)
        {
            var extensao = midia.Extension;
            string opcao = "";
            if (OfflineCoursewareService.Find(x => x.Title == midia.Title) == null)
                opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com a mídia?", "Cancelar", "", null, "Abrir", "Salvar");
            else
                opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com a mídia?", "Cancelar", "", null, "Abrir");

            switch (opcao)
            {
                case "Abrir":
                    if(extensao == "mp4" || extensao == "ogg" || extensao == "webm" || extensao.Contains("videos"))
                    {
                        await MD.Navigation.PushAsync(new PreviewVideoPage(midia.Title, midia.Url));
                    }
                    break;
                case "Salvar":
                    var path = _offlineCourseware.Save(midia.Title, Settings.Email, midia.Url);
                    OfflineCoursewareService.Save(new OfflineCourseware(midia, Settings.ActivationToken, NomePauta, path));                    
                    break;
                default:
                    break;
            }
        }

        private async void OpcoesLink(Link link)
        {
            var abrir = await UserDialogs.Instance.ConfirmAsync("O que deseja fazer com o link?", "", "Abrir", "Cancelar");
            if(abrir)
            {
                await MD.Navigation.PushAsync(new WebViewPage(link.Title, link.Url));
            }
        }

        private async void OpcoesAnexo(Attachment anexo)
        {
            var extensao = anexo.Extension.ToLower();
            string opcao = "";
            if (OfflineCoursewareService.Find(x => x.Title == anexo.Title) == null)
                opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com o material?", "Cancelar", "", null, "Abrir", "Salvar");
            else
                opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com o material?", "Cancelar", "", null, "Abrir");

            switch (opcao)
            {
                case "Abrir":
                    if (extensao == "jpg" || extensao == "png" || extensao == "bitmap" || extensao == "gif")
                    {
                        await MD.Navigation.PushAsync(new PreviewImagePage(anexo.Title, anexo.Url));
                    }
                    else if(extensao == "pdf")
                    {
                        await MD.Navigation.PushAsync(new PreviewPDFPage(anexo.Title, anexo.Url, false));
                    }
                    break;
                case "Salvar":
                    var path = _offlineCourseware.Save(anexo.Title, Settings.Email, anexo.Url);
                    OfflineCoursewareService.Save(new OfflineCourseware(anexo, Settings.ActivationToken, NomePauta, path));                    
                    break;
                default:
                    break;
            }            
        }

        private async void DetalhesForum(Forum forum)
        {
            await MD.Navigation.PushAsync(new MyForumPage(forum.SiteId));
        }
    }
}
