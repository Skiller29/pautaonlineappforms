﻿using pautaonlineforms.Helpers;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class MasterPageViewModel : BaseViewModel
    {
        private string _urlImagem;
        public string UrlImagem
        {
            get { return _urlImagem; }
            set
            {
                _urlImagem = value;
                NotifyPropertyChanged("UrlImagem");
            }
        }

        private string _nome;
        public string Nome
        {
            get { return _nome; }
            set
            {
                _nome = value;
                NotifyPropertyChanged("Nome");
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyPropertyChanged("Email");
            }
        }

        private string _perfil;
        public string Perfil
        {
            get { return _perfil; }
            set
            {
                _perfil = value;
                NotifyPropertyChanged("Perfil");
            }
        }

        private List<string> _opcoes;
        public List<string> Opcoes
        {
            get { return _opcoes; }
            set
            {
                _opcoes = value;
                NotifyPropertyChanged("Opcoes");
            }
        }

        private string _opcao;
        public string Opcao
        {
            get { return _opcao; }
            set
            {
                _opcao = value;
                NotifyPropertyChanged("Opcao");
                if (Opcao != null)
                {
                    DetalhesOpcao(_opcao);
                    Opcao = null;
                }
            }
        }

        public MasterPageViewModel(bool connected = true)
        {
            UrlImagem = Settings.UrlImage;
            Email = Settings.Email;
            Nome = Settings.FullName;
            UrlImagem = Settings.UrlImage;
            Perfil = Settings.Profile;
            if(connected)
                Opcoes = new List<string>() { "Minhas pautas", "Minhas notificações", "Materiais salvos", "Minha conta", "Sair" };
            else
                Opcoes = new List<string>() { "Minhas notificações", "Materiais salvos", "Sair" };

        }

        private async void DetalhesOpcao(string opcao)
        {
            var masterDetail = App.Current.MainPage as MasterDetailPage;

            switch (opcao)
            {
                case "Minhas pautas":
                    break;
                case "Minhas notificações":
                    masterDetail.IsPresented = false;
                    await MD.Navigation.PushAsync(new MyNotificationsPage());
                    break;
                case "Materiais salvos":
                    masterDetail.IsPresented = false;
                    await MD.Navigation.PushAsync(new MyCoursewaresPage());
                    break;
                case "Minha conta":
                    masterDetail.IsPresented = false;
                    await MD.Navigation.PushAsync(new MyAccountPage());
                    break;
                case "Sair":
                    MemberService.Logout();
                    var navigation = new NavigationPage(new IntroductionPage());
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                            break;
                        case Device.Android:
                            navigation.BarTextColor = Color.White;
                            break;
                        default:
                            navigation.BarTextColor = Color.White;
                            break;
                    }
                    App.Current.MainPage = navigation;
                    return;
                default:
                    break;
            }
        }
    }
}
