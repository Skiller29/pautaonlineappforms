﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pautaonlineforms.ViewModels
{
    public class MyNotificationsViewModel : BaseViewModel
    {
        private List<Notification> _notificacoes;
        public List<Notification> Notificacoes
        {
            get { return _notificacoes; }
            set
            {
                _notificacoes = value;
                NotifyPropertyChanged("Notificacoes");
            }
        }

        private Notification _notificacao;
        public Notification Notificacao
        {
            get { return _notificacao; }
            set
            {
                _notificacao = value;
                NotifyPropertyChanged("Notificacao");
                if (Notificacao != null)
                {
                    Notificacao = null;
                }
            }
        }

        public string Pauta { get; set; }

        public MyNotificationsViewModel(string pauta)
        {
            Pauta = pauta;
            CarregarNotificacoes();
        }

        private void CarregarNotificacoes()
        {
            var notificacoes = NotificationsService.ListAll(Settings.ActivationToken).Where(x => x.AgendaName == Pauta).ToList();
            Notificacoes = notificacoes;
        }
    }
}
