﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class MyAccountViewModel : BaseViewModel
    {
        public ICommand SalvarCommand { get; set; }

        private string _nome;
        public string Nome
        {
            get { return _nome; }
            set
            {
                _nome = value;
                NotifyPropertyChanged("Nome");
            }
        }

        private DateTime _dataNascimento;
        public DateTime DataNascimento
        {
            get { return _dataNascimento; }
            set
            {
                _dataNascimento = value;
                NotifyPropertyChanged("DataNascimento");
            }
        }

        private string _celular;
        public string Celular
        {
            get { return _celular; }
            set
            {
                _celular = value;
                NotifyPropertyChanged("Celular");
            }
        }

        private string _cep;
        public string Cep
        {
            get { return _cep; }
            set
            {
                _cep = value;
                NotifyPropertyChanged("Cep");
            }
        }

        private string _endereco;
        public string Endereco
        {
            get { return _endereco; }
            set
            {
                _endereco = value;
                NotifyPropertyChanged("Endereco");
            }
        }

        private string _numero;
        public string Numero
        {
            get { return _numero; }
            set
            {
                _numero = value;
                NotifyPropertyChanged("Numero");
            }
        }

        private string _complemento;
        public string Complemento
        {
            get { return _complemento; }
            set
            {
                _complemento = value;
                NotifyPropertyChanged("Complemento");
            }
        }

        private string _bairro;
        public string Bairro
        {
            get { return _bairro; }
            set
            {
                _bairro = value;
                NotifyPropertyChanged("Bairro");
            }
        }

        private List<string> _estados;
        public List<string> Estados
        {
            get { return _estados; }
            set
            {
                _estados = value;
                NotifyPropertyChanged("Estados");
            }
        }

        private string _estado;
        public string Estado
        {
            get { return _estado; }
            set
            {
                if(!string.IsNullOrWhiteSpace(_estado) && _estado != value)
                    AlterarEstado(value);

                _estado = value;
                NotifyPropertyChanged("Estado");                
                
            }
        }

        private List<string> _cidades;
        public List<string> Cidades
        {
            get { return _cidades; }
            set
            {
                _cidades = value;
                NotifyPropertyChanged("Cidades");
            }
        }

        private string _cidade;
        public string Cidade
        {
            get { return _cidade; }
            set
            {
                _cidade = value;
                NotifyPropertyChanged("Cidade");
            }
        }

        private List<string> _perfis;
        public List<string> Perfis
        {
            get { return _perfis; }
            set
            {
                _perfis = value;
                NotifyPropertyChanged("Perfis");
            }
        }

        private string _perfil;
        public string Perfil
        {
            get { return _perfil; }
            set
            {
                _perfil = value;
                NotifyPropertyChanged("Perfil");
            }
        }

        public List<Profile> PerfisMembro { get; set; }

        public MyAccountViewModel()
        {
            this.SalvarCommand = new Command(Salvar);
            CarregarInformacoes();
        }

        private async void CarregarInformacoes()
        {
            IsBusy = true;
            var membro = await MemberService.FindByActivationToken();
            Perfis = membro.Profiles.Profile.Select(x => x.Name).ToList();
            PerfisMembro = membro.Profiles.Profile;
            Perfil = membro.GetActiveProfile().Name;
            Nome = membro.FullName;
            DataNascimento = membro.BirthDate;
            Endereco = membro.Street;           
            Bairro = membro.Neighborhood;
            Complemento = membro.Complement;
            Numero = membro.Number;
            Cep = membro.Cep;
            Celular = membro.Celular;
            
            var estados = await CityService.ListarEstados();
            var cidades = await CityService.ListarCidades(membro.State);
            Estados = estados.State.Select(x=>x.Name).ToList();
            Cidades = cidades.City.Select(x=>x.Name).ToList();
            Cidade = membro.City;
            Estado = membro.State;
            IsBusy = false;
        }

        private async void Salvar()
        {
            IsBusy = true;
            var perfilAtivo = PerfisMembro.FirstOrDefault(x => x.Name == Perfil);
            var membro = await MemberService.UpdateAccount(Nome, DataNascimento, Cep, Celular, Endereco, Numero, Bairro, Complemento, "", Estado, Cidade, perfilAtivo.EnumSite);
            Settings.FullName = membro.FullName;
            Settings.Profile = membro.GetActiveProfile().Name;
            var index = new IndexPage()
            {
                Master = new Views.MasterPage(),
                Detail = new NavigationPage(new MyAgendasPage()) { Title = "Pauta Online", Icon = "hamburger" },
                IsPresented = false
            };
            App.Current.MainPage = index;
            IsBusy = false;
        }

        private async void AlterarEstado(string estado)
        {
            var cidades = await CityService.ListarCidades(estado);
            Cidades = cidades.City.Select(x => x.Name).ToList();
            Cidade = Cidades.FirstOrDefault();
        }
    }
}
