﻿using pautaonlineforms.Services;
using System.Windows.Input;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class NovaNotificacaoViewModel : BaseViewModel
    {
        public ICommand EnviarCommand { get;set; }

        private string _mensagem;
        public string Mensagem
        {
            get { return _mensagem; }
            set
            {
                _mensagem = value;
                NotifyPropertyChanged("Mensagem");
            }
        }

        public string IdPauta { get; set; }
        public NovaNotificacaoViewModel(string idPauta)
        {
            this.EnviarCommand = new Command(Enviar);
            IdPauta = idPauta;
        }

        private async void Enviar()
        {
            if(string.IsNullOrWhiteSpace(Mensagem))
            {
                ShowMessage("Por favor, verifique se todos os campos foram preenchidos corretamente.");
                return;
            }
            IsBusy = true;
            var notificacao = await NotificationsService.Send(IdPauta, Mensagem);

            await MD.Navigation.PopAsync();
            ShowMessage("Notificação enviada com sucesso.");
            IsBusy = false;
        }
    }
}
