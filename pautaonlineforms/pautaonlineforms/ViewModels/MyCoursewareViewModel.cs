﻿

using Acr.UserDialogs;
using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Interfaces;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class MyCoursewareViewModel : BaseViewModel
    {
        public string Pauta { get; set; }
        private readonly IOfflineCourseware _offlineCourseware;

        private ObservableCollection<OfflineCourseware> _materiais;
        public ObservableCollection<OfflineCourseware> Materiais
        {
            get { return _materiais; }
            set
            {
                _materiais = value;
                NotifyPropertyChanged("Materiais");
            }
        }

        private OfflineCourseware _material;
        public OfflineCourseware Material
        {
            get { return _material; }
            set
            {
                _material = value;
                NotifyPropertyChanged("Material");
                if (Material != null)
                {
                    OpcoesMaterial(_material);
                    Material = null;
                }
            }
        }

        public MyCoursewareViewModel(string pauta)
        {
           Pauta = pauta;
            CarregarMateriais();
            _offlineCourseware = DependencyService.Get<IOfflineCourseware>();
        }
        
        private void CarregarMateriais()
        {
            var materiais = OfflineCoursewareService.ListAll(Settings.ActivationToken).Where(x=>x.AgendaName == Pauta).ToList();
            Materiais = materiais != null && materiais.Any() ? new ObservableCollection<OfflineCourseware>(materiais) : new ObservableCollection<OfflineCourseware>();
        }

        private async void OpcoesMaterial(OfflineCourseware material)
        {
            var extensao = material.Extension;
            var opcao = await UserDialogs.Instance.ActionSheetAsync("O que deseja fazer com o material?", "Cancelar", "", null, "Abrir", "Excluir");
            if(opcao == "Abrir")
            {
                switch (material.Type)
                {
                    case Entities.Enumerations.OfflineCoursewareType.Text:
                        await MD.Navigation.PushAsync(new PreviewTextPage(material.Title, material.Description));
                        break;
                    case Entities.Enumerations.OfflineCoursewareType.Attachment:
                        if (extensao == "jpg" || extensao == "png" || extensao == "bitmap" || extensao == "gif")
                        {
                            await MD.Navigation.PushAsync(new PreviewImagePage(material.Title, material.FilePath));
                        }else if(extensao == "pdf")
                        {                            
                            await MD.Navigation.PushAsync(new PreviewPDFPage(material.Title, material.FilePath, true));
                        }
                        break;
                    case Entities.Enumerations.OfflineCoursewareType.Media:
                        await MD.Navigation.PushAsync(new PreviewVideoPage(material.Title, material.FilePath));
                        break;
                    default:
                        break;
                }
            }
            else
            {
                var confimacao = await UserDialogs.Instance.ConfirmAsync("Tem certeza que deseja excluir este material?");
                if(confimacao)
                {
                    OfflineCoursewareService.Delete(material);
                    if(material.Type != Entities.Enumerations.OfflineCoursewareType.Text)
                    {
                        if (_offlineCourseware.FileExists(material.FilePath))
                            _offlineCourseware.Delete(material.FilePath);
                    }                   

                    Materiais.Remove(material);
                }
                
            }
            
        }
    }
}
