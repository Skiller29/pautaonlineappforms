﻿using System.Threading.Tasks;

namespace pautaonlineforms.ViewModels.Services
{
    public interface IMessageService
    {
        Task ShowAsync(string message);
    }
}
