﻿using pautaonlineforms.Entities;
using pautaonlineforms.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class PostForumViewModel : BaseViewModel
    {
        public ICommand PostarCommand { get; set; }

        private string _mensagem;
        public string Mensagem
        {
            get { return _mensagem; }
            set
            {
                _mensagem = value;
                NotifyPropertyChanged("Mensagem");
            }
        }

        public string IdForum { get; set; }
        public string IdPostagem { get; set; }

        public PostForumViewModel(string idForum, string idPostagem = "")
        {
            IdForum = idForum;
            IdPostagem = idPostagem;
            this.PostarCommand = new Command(Postar);
        }

        private async void Postar()
        {
            Forum postagem = new Forum();
            IsBusy = true;
            if (string.IsNullOrWhiteSpace(IdPostagem))
            {
                postagem = await ForumService.Post(IdForum, Mensagem);
            }
            else
            {
                postagem = await ForumService.ReplyPost(IdForum, Mensagem, IdPostagem);
            }
            IsBusy = false;
            await MD.Navigation.PopAsync();
            MessagingCenter.Send(postagem, "RefreshForum");
        }
    }
}
