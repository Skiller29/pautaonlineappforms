﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Services;
using pautaonlineforms.ViewModels.Services;
using pautaonlineforms.Views;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public ICommand LoginCommand { get; set; }
        private readonly IMessageService _messageService;

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                NotifyPropertyChanged("Email");
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyPropertyChanged("Password");
            }
        }

        public LoginViewModel()
        {
            this.LoginCommand = new Command(Login);
        }

        private async void Login()
        {
            if(string.IsNullOrWhiteSpace(Email) || string.IsNullOrWhiteSpace(Password))
            {
                ShowMessage("Por favor, verifique se todos os campos foram preenchidos corretamente.");
                return;
            }
            IsBusy = true;
            if (!CrossConnectivity.Current.IsConnected)
            {
                var member = OfflineMemberService.Authenticate(Email, Password);
                if(member != null)
                {
                    Settings.ActivationToken = member.ActivationToken;
                    Settings.FullName = member.FullName;
                    Settings.Profile = member.ActiveProfile;
                    Settings.Email = member.Email;

                    var navigation = new NavigationPage(new MyCoursewaresPage());
                    navigation.Icon = "hamburger";
                    navigation.Title = Settings.ProjectName;
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                            break;
                        case Device.Android:
                            navigation.BarTextColor = Color.White;
                            break;
                        default:
                            navigation.BarTextColor = Color.White;
                            break;
                    }
                    var index = new IndexPage()
                    {
                        Master = new Views.MasterPage(false) { Title = Settings.ProjectName },
                        Detail = navigation,
                        IsPresented = false
                    };
                    App.Current.MainPage = index;
                }
                else
                {
                    ShowMessage("Usuário não encontrado. Por favor, verifique os dados e tente novamente.");
                }
            }
            else
            {
                var member = await MemberService.Authenticate(Email, Password);
                if (string.IsNullOrWhiteSpace(member.Error))
                {
                    Settings.ActivationToken = member.ActivationToken;
                    Settings.FullName = member.FullName;
                    Settings.Profile = member.GetActiveProfile().Name;
                    Settings.UrlImage = member.PhotoUrl;
                    Settings.Email = member.Email;
                    OfflineMemberService.SaveOrUpdate(new OfflineMember(member, Password));

                    var navigation = new NavigationPage(new MyAgendasPage());
                    navigation.Icon = "hamburger";
                    navigation.Title = Settings.ProjectName;
                    switch (Device.RuntimePlatform)
                    {
                        case Device.iOS:
                            navigation.BarTextColor = Color.FromHex(Settings.PrimaryColor);
                            break;
                        case Device.Android:
                            navigation.BarTextColor = Color.White;
                            break;
                        default:
                            navigation.BarTextColor = Color.White;
                            break;
                    }

                    var index = new IndexPage()
                    {
                        Master = new Views.MasterPage() { Title = Settings.ProjectName},
                        Detail = navigation,
                        IsPresented = false
                    };
                    App.Current.MainPage = index;
                }
                else
                {
                    ShowMessage(member.Error);
                }
            }
              
            IsBusy = false;
        }
    }
}
