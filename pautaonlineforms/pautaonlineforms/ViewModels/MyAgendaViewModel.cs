﻿using pautaonlineforms.Entities;
using pautaonlineforms.Helpers;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class MyAgendaViewModel : BaseViewModel
    {
        public ICommand NotificacaoCommand { get; set; }

        private string _tituloNotificacao;
        public string TituloNotificacao
        {
            get { return _tituloNotificacao; }
            set
            {
                _tituloNotificacao = value;
                NotifyPropertyChanged("TituloNotificacao");
            }
        }

        private Agenda _pauta;
        public Agenda Pauta
        {
            get { return _pauta; }
            set
            {
                _pauta = value;
                NotifyPropertyChanged("Pauta");
            }
        }

        private List<Class> _aulas;
        public List<Class> Aulas
        {
            get { return _aulas; }
            set
            {
                _aulas = value;
                NotifyPropertyChanged("Aulas");
            }
        }

        private Class _aula;
        public Class Aula
        {
            get { return _aula; }
            set
            {
                _aula = value;
                NotifyPropertyChanged("Aula");
                if (Aula != null)
                {
                    DetalhesAula(_aula);
                    Aula = null;
                }
            }
        }

        private List<Forum> _foruns;
        public List<Forum> Foruns
        {
            get { return _foruns; }
            set
            {
                _foruns = value;
                NotifyPropertyChanged("Foruns");
            }
        }

        private Forum _forum;
        public Forum Forum
        {
            get { return _forum; }
            set
            {
                _forum = value;
                NotifyPropertyChanged("Forum");
                if (Forum != null)
                {
                    DetalhesForum(_forum);
                    Forum = null;
                }
            }
        }

        public MyAgendaViewModel(string idAgenda)
        {
            TituloNotificacao = Settings.Profile != "Aluno" ? "Enviar notificação" : "";
            DetalhesPauta(idAgenda);
            this.NotificacaoCommand = new Command(EnviarNotificacao);
        }

        private async void DetalhesPauta(string idAgenda)
        {
            IsBusy = true;
            var pauta = await AgendasService.FindByIdAndActivationToken(idAgenda);
            Pauta = pauta;
            var teste = pauta.Objectives.PlainText(pauta.Objectives.Length);
            if(Settings.Profile == "Aluno")
            {
                Foruns = pauta.Forums != null && pauta.Forums.Forum.Any() ? pauta.Forums.Forum.Where(x=>x.IsVisible).ToList() : new List<Forum>();
                Aulas = pauta.Classes != null && pauta.Classes.Class.Any() ? pauta.Classes.Class.Where(x => x.IsVisible).ToList() : new List<Class>();
            }
            else
            {
                Foruns = pauta.Forums != null && pauta.Forums.Forum.Any() ? pauta.Forums.Forum : new List<Forum>();
                Aulas = pauta.Classes != null && pauta.Classes.Class.Any() ? pauta.Classes.Class : new List<Class>();
            }
            
            IsBusy = false;
        }

        private async void DetalhesAula(Class aula)
        {
            await MD.Navigation.PushAsync(new MyClassPage(aula.SiteId, Pauta.Name));
        }

        private async void DetalhesForum(Forum forum)
        {
            await MD.Navigation.PushAsync(new MyForumPage(forum.SiteId));
        }

        private async void EnviarNotificacao()
        {
            if(Settings.Profile != "Aluno")
                await MD.Navigation.PushAsync(new NovaNotificacaoPage(Pauta.SiteId));
        }
    }
}
