﻿using pautaonlineforms.Entities;
using pautaonlineforms.Views;
using System.Collections.Generic;
using System.Linq;

namespace pautaonlineforms.ViewModels
{
    public class MyAgendasInstituteViewModel : BaseViewModel
    {
        private List<Agenda> _agendas;
        public List<Agenda> Agendas
        {
            get { return _agendas; }
            set
            {
                _agendas = value;
                NotifyPropertyChanged("Agendas");
            }
        }

        private Agenda _agenda;
        public Agenda Agenda
        {
            get { return _agenda; }
            set
            {
                _agenda = value;
                NotifyPropertyChanged("Agenda");
                if (Agenda != null)
                {
                    DetalhesPauta(_agenda);
                    Agenda = null;
                }
            }
        }

        public List<Hierarchy> Hierarchies { get; set; }
        public MyAgendasInstituteViewModel(List<Hierarchy> hierarchies)
        {
            Hierarchies = hierarchies;
            CarregarPautas();
        }

        private void CarregarPautas()
        {
            IsBusy = true;
            this.Agendas = new List<Agenda>();
            foreach (var hierarchy in Hierarchies)
            {
                if(hierarchy.Agendas != null && hierarchy.Agendas.Agenda.Any())
                {
                    foreach (var agenda in hierarchy.Agendas.Agenda)
                    {
                        Agendas.Add(agenda);
                    }
                }
                
                if(hierarchy.SubHierarchy != null && hierarchy.SubHierarchy.Any())
                {
                    foreach (var subhierarchy in hierarchy.SubHierarchy)
                    {
                        if (subhierarchy.Agendas != null && subhierarchy.Agendas.Agenda.Any())
                        {
                            foreach (var agenda in subhierarchy.Agendas.Agenda)
                            {
                                Agendas.Add(agenda);
                            }
                        }     
                    }
                }
            }
            IsBusy = false;
        }

        private async void DetalhesPauta(Agenda pauta)
        {
            await MD.Navigation.PushAsync(new MyAgendaPage(pauta.SiteId));            
        }
    }
}
