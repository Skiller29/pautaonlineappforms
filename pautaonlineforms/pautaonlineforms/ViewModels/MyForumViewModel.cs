﻿using Acr.UserDialogs;
using pautaonlineforms.Entities;
using pautaonlineforms.Services;
using pautaonlineforms.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class MyForumViewModel : BaseViewModel
    {
        public ICommand PostarCommand { get; set; }

        private ObservableCollection<ForumMessage> _mensagens;
        public ObservableCollection<ForumMessage> Mensagens
        {
            get { return _mensagens; }
            set
            {
                _mensagens = value;
                NotifyPropertyChanged("Mensagens");
            }
        }

        private ForumMessage _mensagem;
        public ForumMessage Mensagem
        {
            get { return _mensagem; }
            set
            {
                _mensagem = value;
                NotifyPropertyChanged("Mensagem");
                if (Mensagem != null)
                {
                    Responder(_mensagem);
                    Mensagem = null;
                }
            }
        }

        private string _nomeForum;
        public string NomeForum
        {
            get { return _nomeForum; }
            set
            {
                _nomeForum = value;
                NotifyPropertyChanged("NomeForum");
            }
        }

        public string IdForum { get; set; }

        public MyForumViewModel(string idForum)
        {
            IdForum = idForum;
            CarregarMensagens(idForum);
            PostarCommand = new Command(Postar);
        }

        private async void CarregarMensagens(string idForum)
        {
            IsBusy = true;
            var forum = await ForumService.FindByIdAndActivationToken(idForum);
            Mensagens = forum.ForumMessages != null && forum.ForumMessages.ForumMessage.Any() ? new ObservableCollection<ForumMessage>(forum.ForumMessages.ForumMessage) : new ObservableCollection<ForumMessage>();
            NomeForum = forum.Title;
            IsBusy = false;

            MessagingCenter.Subscribe<Forum>(this, "RefreshForum", (post) =>
            {
                CarregarMensagens(IdForum);
            });
        }

        private async void Postar()
        {
            await MD.Navigation.PushAsync(new PostForumPage(IdForum));
        }

        private async void Responder(ForumMessage post)
        {
            var opcao = await UserDialogs.Instance.ConfirmAsync("Deseja responder esta postagem?", "", "Sim", "Não");
            if(opcao)
                await MD.Navigation.PushAsync(new PostForumPage(IdForum, post.SiteId));
        }
    }
}
