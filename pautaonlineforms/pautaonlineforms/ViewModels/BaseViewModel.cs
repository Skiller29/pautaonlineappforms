﻿using Acr.UserDialogs;
using pautaonlineforms.Helpers;
using pautaonlineforms.ViewModels.Services;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace pautaonlineforms.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {

        private string _nomeProjeto;
        public string NomeProjeto
        {
            get { return _nomeProjeto; }
            set
            {
                _nomeProjeto = value;
                NotifyPropertyChanged("NomeProjeto");
            }
        }

        private string _corProjeto;
        public string CorProjeto
        {
            get { return _corProjeto; }
            set
            {
                _corProjeto = value;
                NotifyPropertyChanged("CorProjeto");
            }
        }

        private string _logoProjeto;
        public string LogoProjeto
        {
            get { return _logoProjeto; }
            set
            {
                _logoProjeto = value;
                NotifyPropertyChanged("LogoProjeto");
            }
        }

        public BaseViewModel()
        {
            NomeProjeto = Settings.ProjectName;
            CorProjeto = Settings.PrimaryColor;
            LogoProjeto = Settings.LogoName;
        }

        protected bool ChangeAndNotify<T>(ref T property, T value, [CallerMemberName] string propertyName = "")
        {
            if (!EqualityComparer<T>.Default.Equals(property, value))
            {
                property = value;
                NotifyPropertyChanged(propertyName);
                return true;
            }


            return false;
        }

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (_isBusy != value)
                {
                    _isBusy = value;
                    NotifyPropertyChanged("IsBusy");
                    Loading();
                }

            }
        }

        public async void Loading()
        {
            if (IsBusy)
                 UserDialogs.Instance.ShowLoading("Carregando", MaskType.Clear);
            else
                UserDialogs.Instance.HideLoading();
        }

        public Page MD
        {
            get
            {
                var detail = App.Current.MainPage as MasterDetailPage;
                return detail.Detail;
            }
        }

        public void ShowMessage(string message)
        {
            UserDialogs.Instance.Toast(message, new TimeSpan(0, 0, 4));
        }

        public async Task<bool> Disconnected()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                ShowMessage("Você está desconectado. Verifique sua conexão.");
                return true;
            }

            return false;
        }
    }
}
