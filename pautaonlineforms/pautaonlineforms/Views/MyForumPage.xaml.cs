﻿using pautaonlineforms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyForumPage : ContentPage
    {
        public MyForumPage(string idForum)
        {
            InitializeComponent();
            this.BindingContext = new MyForumViewModel(idForum);
        }
    }
}