﻿using pautaonlineforms.ViewModels;
using Rox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreviewVideoPage : ContentPage
    {
        public PreviewVideoPage(string titulo, string url)
        {
            InitializeComponent();
            Title = titulo;
            this.BindingContext = new PreviewVideoViewModel(GetVideoView(), url);
        }

        public VideoView GetVideoView()
        {
            return VideoView;
        }
    }
}