﻿using pautaonlineforms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NovaNotificacaoPage : ContentPage
    {
        public NovaNotificacaoPage(string idAgenda)
        {
            InitializeComponent();
            this.BindingContext = new NovaNotificacaoViewModel(idAgenda);
        }
    }
}