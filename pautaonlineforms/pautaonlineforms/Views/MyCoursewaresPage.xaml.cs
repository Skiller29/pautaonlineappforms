﻿using pautaonlineforms.Helpers;
using pautaonlineforms.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyCoursewaresPage : TabbedPage
    {
        public MyCoursewaresPage()
        {
            InitializeComponent();
            var pautas = OfflineCoursewareService.ListAll(Settings.ActivationToken).Select(x => x.AgendaName).Distinct().ToList();
            if(pautas != null && pautas.Any())
            {
                foreach (var pauta in pautas)
                {
                    this.Children.Add(new MyCoursewarePage(pauta));
                }
            }
            else
            {
                var contentPage = new ContentPage();
                var stackLayout = new StackLayout();
                stackLayout.VerticalOptions = LayoutOptions.Center;
                stackLayout.HorizontalOptions = LayoutOptions.Center;
                var Label = new Label() { Text = "Não existem materiais salvos."};
                stackLayout.Children.Add(Label);
                contentPage.Content = stackLayout;
                this.Children.Add(contentPage);
            }           
        }
    }
}