﻿using Acr.UserDialogs;
using pautaonlineforms.Interfaces;
using Syncfusion.SfPdfViewer.XForms;
using System;
using System.Globalization;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreviewPDFPage : ContentPage
    {
        private readonly IOfflineCourseware _offlineCourseware;
        public string Url { get; set; }
        public bool IsLocal { get; set; }
        public PreviewPDFPage(string titulo, string url, bool isLocal)
        {
            InitializeComponent();
            Url = url;
            IsLocal = isLocal;
            Title = titulo;
            _offlineCourseware = DependencyService.Get<IOfflineCourseware>();
            goToPreviousButton.Clicked += OnGoToPreviousPageClicked;
            goToNextButton.Clicked += OnGoToNextPageClicked;
            pageNumberEntry.Completed += CurrentPageEntry_Completed;
            pdfViewerControl.PageChanged += PdfViewerControl_PageChanged;
            pageNumberEntry.Text = "1";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if(IsLocal)
            {
                Stream documenStream = DependencyService.Get<IOfflineCourseware>().GetStreamPDF(Url);
                pdfViewerControl.LoadDocument(documenStream);
            }
            else
            {
                Stream documenStream = DependencyService.Get<IDownloader>().DownloadPdfStream(Url, "documentpdftemp");
                pdfViewerControl.LoadDocument(documenStream);
            }
           
            pageCountLabel.Text = pdfViewerControl.PageCount.ToString();
        }

        private void PdfViewerControl_PageChanged(object sender, PageChangedEventArgs args)
        {
            pageNumberEntry.Text = args.PageNumber.ToString();
        }

        private void CurrentPageEntry_Completed(object sender, EventArgs e)
        {
            int pageNumber = 1;
            if (int.TryParse(((sender as Entry).Text), NumberStyles.Integer, CultureInfo.InvariantCulture, out pageNumber))
            {
                if ((sender as Entry) != null && pageNumber > 0 && pageNumber <= pdfViewerControl.PageCount)
                    pdfViewerControl.GoToPage(int.Parse((sender as Entry).Text));
                else
                {
                    DisplayAlert("Alert", "Please enter the valid page number.", "OK");
                    (sender as Entry).Text = pdfViewerControl.PageNumber.ToString();
                }
            }
            else
            {
                DisplayAlert("Alert", "Please enter the valid page number.", "OK");
                (sender as Entry).Text = pdfViewerControl.PageNumber.ToString();
            }

        }

        void OnGoToPreviousPageClicked(object sender, EventArgs e)
        {
            //Navigates to previous page of the PDF document.
            pdfViewerControl.GoToPreviousPage();
        }

        void OnGoToNextPageClicked(object sender, EventArgs e)
        {
            //Navigates to the next page of the PDF document.
            pdfViewerControl.GoToNextPage();
        }
    }
}