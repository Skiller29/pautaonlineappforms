﻿using pautaonlineforms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyClassPage : TabbedPage
    {
        public MyClassPage(string idAula, string nomePauta)
        {
            InitializeComponent();
            this.BindingContext = new MyClassViewModel(idAula, nomePauta);
        }
    }
}