﻿using pautaonlineforms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostForumPage : ContentPage
    {
        public PostForumPage(string idForum, string idPostagem = "")
        {
            InitializeComponent();
            this.BindingContext = new PostForumViewModel(idForum, idPostagem);
        }
    }
}