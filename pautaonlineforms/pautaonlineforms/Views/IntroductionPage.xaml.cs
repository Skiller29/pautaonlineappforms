﻿using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntroductionPage : CarouselPage
    {
        public IntroductionPage()
        {
            InitializeComponent();
            Title = Settings.ProjectName;
            logo.Source = Settings.LogoName;
            btnLogin.BackgroundColor = Color.FromHex(Settings.PrimaryColor);
            btnLoginFacebook.BackgroundColor = Color.FromHex(Settings.PrimaryColor);
        }

        private void Login_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage.Navigation.PushAsync(new LoginPage());
        }

        private void Login_Facebook_Clicked(object sender, EventArgs e)
        {
            App.Current.MainPage.Navigation.PushAsync(new LoginFacebookPage());
        }
    }
}