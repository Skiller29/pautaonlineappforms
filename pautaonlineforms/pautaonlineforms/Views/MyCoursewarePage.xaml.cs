﻿using pautaonlineforms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyCoursewarePage : ContentPage
    {
        public MyCoursewarePage(string nomePauta)
        {
            InitializeComponent();
            Title = nomePauta;
            this.BindingContext = new MyCoursewareViewModel(nomePauta);
        }
    }
}