﻿using pautaonlineforms.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreviewTextPage : ContentPage
    {
        public PreviewTextPage(string titulo, string texto)
        {
            InitializeComponent();
            Texto.Text = texto.RemoveCData().RemoveHtmlTags() ;
            Title = titulo;
        }
    }
}