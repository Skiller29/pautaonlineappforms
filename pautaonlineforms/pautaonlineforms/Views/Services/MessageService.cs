﻿using System;
using System.Threading.Tasks;
using Acr.UserDialogs;

namespace pautaonlineforms.Views.Services
{
    public class MessageService : ViewModels.Services.IMessageService
    {
        public async Task ShowAsync(string message)
        {
            UserDialogs.Instance.Toast(message, new TimeSpan(0,0,4));
        }
    }
}
