﻿using pautaonlineforms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyNotificationPage : ContentPage
    {
        public MyNotificationPage(string nomePauta)
        {
            InitializeComponent();
            Title = nomePauta;
            this.BindingContext = new MyNotificationsViewModel(nomePauta);
        }
    }
}