﻿using Acr.UserDialogs;
using pautaonlineforms.Entities;
using pautaonlineforms.Services;
using pautaonlineforms.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace pautaonlineforms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyAgendasPage : TabbedPage
    {
        public Member Membro { get; set; }
        public MyAgendasPage(Member member = null)
        {
            InitializeComponent();
            Membro = member;
            CarregarPautas();
        }

        private async void CarregarPautas()
        {
           //UserDialogs.Instance.ShowLoading("Carregando", MaskType.Clear);

            if (Membro == null)
                Membro = await MemberService.ListStructure();
            
            foreach (var institute in Membro.Institutes.Institute)
            {
                this.Children.Add(new MyAgendasInstitutePage()
                {
                    BindingContext = new MyAgendasInstituteViewModel(institute.Hierarchies.Hierarchy),
                    Title = institute.Name
                });
            }

            //UserDialogs.Instance.HideLoading();
        }
    }
}