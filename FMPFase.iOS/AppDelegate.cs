﻿using Foundation;
using UIKit;
using pautaonlineforms;
using Syncfusion.SfPdfViewer.XForms.iOS;
using PushNotification.Plugin;
using Plugin.HtmlLabel.iOS;

namespace FMPFase.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            HtmlLabelRenderer.Initialize();
            Rox.VideoIos.Init();
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());
            CrossPushNotification.Initialize<CrossPushNotificationListener>();
            new SfPdfDocumentViewRenderer();
            return base.FinishedLaunching(app, options);
        }
    }


}