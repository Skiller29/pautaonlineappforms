﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using pautaonlineforms.Interfaces;
using pautaonlineforms.Entities;
using FMPFase.iOS;

[assembly: Xamarin.Forms.Dependency(typeof(ClientAppImplementation))]
namespace FMPFase.iOS
{
    public class ClientAppImplementation : IClientApp
    {
        public ClientApp GetClient()
        {
            var cliente = new ClientApp()
            {
                AppName = "FMP FASE",
                HexaAppColor = "#00706B",
                IdInstitute = 1,
                LogoName = "logo_fmpfase"
            };

            return cliente;
        }
    }
}