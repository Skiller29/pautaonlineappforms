﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using System.IO;
using pautaonlineforms.Interfaces;
using Acr.UserDialogs;
using System.Net;
using System.ComponentModel;
using Xamarin.Forms;
using FMPFase.iOS.Files;

[assembly: Dependency(typeof(OfflineCourseware_iOS))]
namespace FMPFase.iOS.Files
{
   public class OfflineCourseware_iOS : IOfflineCourseware
    {
        public static string DocumentsPath
        {
            get
            {
                var documentsDirUrl = NSFileManager.DefaultManager.GetUrls(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User).Last();
                return documentsDirUrl.Path;
            }
        }

        #region ISaveAndLoad implementation

        public string Save(string filename, string email, string url)
        {
            var name = Path.Combine(email, filename);
            var folderPath = CreatePathToFile(email);
            var path = CreatePathToFile(name);

            if (!Directory.Exists(CreatePathToFile(email)))
                Directory.CreateDirectory(CreatePathToFile(email));

            Download(url, path);
            return path;
        }

        public string Load(string filename)
        {
            if (FileExists(filename))
                return CreatePathToFile(filename);

            return null;
        }

        public bool FileExists(string filename)
        {
            return File.Exists(CreatePathToFile(filename));
        }

        public void Delete(string filename)
        {
            var path = CreatePathToFile(filename);
            if (FileExists(path))
                File.Delete(path);
        }

        public Stream GetStreamPDF(string url)
        {
            using (WebClient myWebClient = new WebClient())
            {
                return myWebClient.OpenRead(new Uri(url));
            }
        }
        #endregion

        static string CreatePathToFile(string fileName)
        {
            return Path.Combine(DocumentsPath, fileName);
        }

        void Download(string url, string path)
        {
            using (WebClient myWebClient = new WebClient())
            {
                myWebClient.DownloadFileCompleted += DownloadCompleted;
                myWebClient.DownloadFileAsync(new Uri(url), path);
                UserDialogs.Instance.Toast("O download do material começou.", new TimeSpan(0, 0, 4));
            }
        }

        public static void DownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            UserDialogs.Instance.Toast("Download completo", new TimeSpan(0, 0, 4));
        }

       
    }
}