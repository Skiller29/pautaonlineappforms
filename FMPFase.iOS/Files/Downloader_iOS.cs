﻿using System;
using pautaonlineforms.Interfaces;
using System.IO;
using System.Net;
using Xamarin.Forms;
using Library.iOS.Files;

[assembly: Dependency(typeof(Downloader_iOS))]
namespace Library.iOS.Files
{
    public class Downloader_iOS : IDownloader
    {
        WebClient m_webClient = new WebClient();
        string filename;
        Stream documentStream;

        public Downloader_iOS()
        {
        }

        public Stream DownloadPdfStream(string URL, string documentName)
        {
            var uri = new System.Uri(URL);

            //Returns the PDF document stream from the given URL
            documentStream = m_webClient.OpenRead(uri);

            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filePath = Path.Combine(path, documentName + ".pdf");
            try
            {
                FileStream fileStream = File.Open(filePath, FileMode.Create);
                //docstream.Position = 0;
                documentStream.CopyTo(fileStream);
                fileStream.Flush();
                fileStream.Close();
            }
            catch (Exception e)
            {

            }

            if (File.Exists(filePath))
            {
                return new MemoryStream(File.ReadAllBytes(filePath));
            }
            return documentStream;
        }
    }
}