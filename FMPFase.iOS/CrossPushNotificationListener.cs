﻿using System;
using System.Collections.Generic;
using PushNotification.Plugin;
using PushNotification.Plugin.Abstractions;
using pautaonlineforms;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using pautaonlineforms.Services;
using UIKit;

namespace FMPFase.iOS
{
    public class CrossPushNotificationListener : IPushNotificationListener
    {
        public void OnError(string message, DeviceType deviceType)
        {
            //throw new NotImplementedException();
        }

        public void OnMessage(JObject values, DeviceType deviceType)
        {
            try
            {
                //var sound = Android.Net.Uri.Parse("android.resource://com.myPackageName.org/" + Resource.Drawable.notification);               
                //Json Response Recieved 
                Dictionary<string, string> results = JsonConvert.DeserializeObject<Dictionary<string, string>>(values.ToString());
                var notificacao = new pautaonlineforms.Entities.Notification()
                {
                    AgendaName = results["AgendaName"],
                    SenderName = results["SenderName"],
                    Content = results["Content"],
                    SentIn = DateTime.Now

                };

                var notification = new UILocalNotification();
                notification.AlertAction = $"{notificacao.SenderName} - {notificacao.AgendaName}";
                notification.AlertBody = notificacao.Content;
                notification.ApplicationIconBadgeNumber = notification.ApplicationIconBadgeNumber + 1;
                notification.SoundName = UILocalNotification.DefaultSoundName;
                UIApplication.SharedApplication.ScheduleLocalNotification(notification);

                NotificationsService.Save(notificacao);
            }
            catch (Exception)
            {
            }
        }

        public void OnRegistered(string token, DeviceType deviceType)
        {
            App.GetToken(token, "iOS");
        }

        public void OnUnregistered(DeviceType deviceType)
        {
            //throw new NotImplementedException();
        }

        public bool ShouldShowNotification()
        {
            return false;
        }
    }
}