﻿using System;
using Xamarin.Forms.Platform.iOS;
using pautaonlineforms.Views;
using Library.iOS;
using Xamarin.Forms;
using Xamarin.Auth;
using pautaonlineforms;
using Newtonsoft.Json.Linq;
using FMPFase.iOS;

[assembly: ExportRenderer(typeof(LoginFacebookPage), typeof(LoginFacebookCustomRenderer))]
namespace FMPFase.iOS
{
    public class LoginFacebookCustomRenderer : PageRenderer
    {
        bool done = false;
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (done)
                return;

            var auth = new OAuth2Authenticator(
           clientId: "212660225441375", // your OAuth2 client id
           scope: "email", // the scopes for the particular API you're accessing, delimited by "+" symbols
           authorizeUrl: new Uri("https://www.facebook.com/v2.8/dialog/oauth/"),
           redirectUrl: new Uri("https://www.facebook.com/connect/login_success.html"));

            auth.Completed += async (sender, eventArgs) =>
            {
                DismissViewController(true, null);

                App.HideLoginView();

                if (eventArgs.IsAuthenticated)
                {
                    var accessToken = eventArgs.Account.Properties["access_token"].ToString();
                    var expiresIn = Convert.ToDouble(eventArgs.Account.Properties["expires_in"]);
                    var expiryDate = DateTime.Now + TimeSpan.FromSeconds(expiresIn);

                    var request = new OAuth2Request("GET",
                     new Uri("https://graph.facebook.com/me?fields=id,email,name,picture"), null, eventArgs.Account);
                    var response = await request.GetResponseAsync();
                    var obj = JObject.Parse(response.GetResponseText());

                    var id = obj["id"].ToString().Replace("\"", "");
                    var email = obj["email"].ToString().Replace("\"", "");
                    var name = obj["name"].ToString().Replace("\"", "");
                    var foto = "http://graph.facebook.com/" + id + "/picture?type=large";

                    await App.AuthenticationFacebook(id, name, email, foto);
                }
                else
                {
                    App.Current.MainPage = new NavigationPage(new IntroductionPage()); ;
                }
            };

            done = true;
            PresentViewController(auth.GetUI(), true, null);
        }
    }
}