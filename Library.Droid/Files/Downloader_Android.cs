﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using pautaonlineforms.Interfaces;
using System.IO;
using System.Net;
using Xamarin.Forms;
using Library.Droid.Files;

[assembly: Dependency(typeof(Downloader_Android))]
namespace Library.Droid.Files
{
    public class Downloader_Android : IDownloader
    {
        WebClient m_webClient = new WebClient();
        string filename;
        Stream documentStream;

        public Downloader_Android()
        {
        }

        public Stream DownloadPdfStream(string URL, string documentName)
        {
            var uri = new System.Uri(URL);

            //Returns the PDF document stream from the given URL
            documentStream = m_webClient.OpenRead(uri);

            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filePath = Path.Combine(path, documentName + ".pdf");
            try
            {
                FileStream fileStream = File.Open(filePath, FileMode.Create);
                //docstream.Position = 0;
                documentStream.CopyTo(fileStream);
                fileStream.Flush();
                fileStream.Close();
            }
            catch (Exception e)
            {

            }

            if (File.Exists(filePath))
            {
                return new MemoryStream(File.ReadAllBytes(filePath));
            }
            return documentStream;
        }
    }
}