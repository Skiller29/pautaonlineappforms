﻿using System;
using pautaonlineforms.Interfaces;
using System.IO;
using System.Net;
using System.ComponentModel;
using Acr.UserDialogs;
using Library.Droid.Files;
using Xamarin.Forms;

[assembly: Dependency(typeof(OfflineCourseware_Android))]
namespace Library.Droid.Files
{
    public class OfflineCourseware_Android : IOfflineCourseware
    {
        public bool FileExists(string filename)
        {
            return File.Exists(CreatePathToFile(filename));
        }

        public string Load(string filename)
        {
            if(FileExists(filename))
                return CreatePathToFile(filename);

            return null;
        }

        public string Save(string filename, string email, string url)
        {
            var name = Path.Combine(email, filename);
            var folderPath = CreatePathToFile(email);
            var path = CreatePathToFile(name);

            if (!Directory.Exists(CreatePathToFile(email)))
                Directory.CreateDirectory(CreatePathToFile(email));

            Download(url, path);
            return path;
        }

        string CreatePathToFile(string filename)
        {
            var docsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(docsPath, filename);
        }

        void Download(string url, string path)
        {
            using (WebClient myWebClient = new WebClient())
            {
                myWebClient.DownloadFileCompleted += DownloadCompleted;
                myWebClient.DownloadFileAsync(new Uri(url), path);
                UserDialogs.Instance.Toast("O download do material começou.", new TimeSpan(0, 0, 4));
            }
        }

        public void Delete(string filename)
        {
            var path = CreatePathToFile(filename);
            if (FileExists(path))
                File.Delete(path);
        }

        public static void DownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            UserDialogs.Instance.Toast("Download completo", new TimeSpan(0, 0, 4));
        }

        public Stream GetStreamPDF(string path)
        {
            if(FileExists(CreatePathToFile(path)))
            {
                var stream = File.Open(CreatePathToFile(path), FileMode.Open);
                return stream;
            }

            return null;
        }
    }
}